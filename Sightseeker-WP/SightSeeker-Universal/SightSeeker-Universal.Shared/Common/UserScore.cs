﻿namespace SightSeeker_Universal.Common
{
    public class UserScore
    {
        public UserScore(int id, int score)
        {
            Id = id;
            Score = score;
        }

        public int Id { get; set; }
        public int Score { get; set; }
    }
}