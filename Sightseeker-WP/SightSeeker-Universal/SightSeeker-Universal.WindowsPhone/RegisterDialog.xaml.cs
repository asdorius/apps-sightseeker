﻿#region usings

using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using SightSeeker_Universal.Common;

#endregion

// Die Elementvorlage "Inhaltsdialog" ist unter "http://go.microsoft.com/fwlink/?LinkID=390556" dokumentiert.

namespace SightSeeker_Universal
{
    public sealed partial class RegisterDialog : ContentDialog
    {
        public RegisterDialog()
        {
            InitializeComponent();
        }

        private void ContentDialog_SignInButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            if (password.Password == passwordRepeat.Password)
            {
                try
                {
                    Backend.RegisterUser(email.Text, password.Password);
                }
                catch (UserAllreadyExistsException)
                {
                    new MessageDialog("User allready exists", "Warning!").ShowAsync();
                    args.Cancel = true;
                }
                catch
                {
                    args.Cancel = true;
                }
            }
            else
            {
                args.Cancel = true;
                passwordRepeat.BorderBrush = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
                passwordRepeat.BorderThickness = new Thickness(3);
            }
        }

        private void ContentDialog_CancelButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}