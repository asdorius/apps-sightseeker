package de.horb.dhbw.sightseeker.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import java.util.List;

import de.horb.dhbw.sightseeker.activity.NavigationDrawerActivity;
import de.horb.dhbw.sightseeker.adapter.PlaceListAdapter;
import de.horb.dhbw.sightseeker.data.Place;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.manager.GPSManager;
import de.horb.dhbw.sightseeker.manager.WebserviceManager;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.support.Support;
import de.horb.dhbw.sightseeker.webservice.App_Tag;
import de.horb.dhbw.sightseeker.webservice.WebserviceTaskResult;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class PlacesFragment extends Fragment implements AbsListView.OnItemClickListener {

    private OnFragmentInteractionListener mListener;
    private AbsListView mListView;

    private View mProgressView;
    private View mFormView;

    private PlaceListAdapter mAdapter;
    private DatabaseHandler mDbHandler;

    private String  mNfcData;
    private Place   mNewPlace;

    // Maximum difference of latitude and longitude (correlates to ~10 meter)
    private static final Double MAX_DIFF_MINUS = -0.0001;
    private static final Double MAX_DIFF_PLUS = 0.0001;

    public static PlacesFragment newInstance(int sectionNumber) {
        PlacesFragment fragment = new PlacesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    // Constructor for passing nfc data by intent
    public static PlacesFragment newInstance(int sectionNumber, Intent intent) {
        PlacesFragment fragment = new PlacesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        args.putString(Constants.ARG_NFC_DATA, intent.getData().toString());
        fragment.setArguments(args);
        return fragment;
    }

    public PlacesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create database if not exists and get writable access
        mDbHandler = new DatabaseHandler(getActivity());
        mDbHandler.openDataBase();

        List<Place> places = mDbHandler.getAllPlaces();

        // Set up the ListAdapter
        mAdapter = new PlaceListAdapter(getActivity(), R.layout.place_item, places);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_places, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//            }
//        });

        mProgressView = view.findViewById(R.id.progressBar_places);
        mFormView = view.findViewById(R.id.layout_places);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Handle nfc data if nfc was detected
        if (isNFCAction()) {
            handleNFC();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationDrawerActivity) activity).onSectionAttached(getArguments().getInt(Constants.ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mListener != null) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
//            mListener.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragment contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragment/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

    private NavigationDrawerActivity getParentActivity() {
        NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity) getActivity();
        return navigationDrawerActivity;
    }

    private boolean isNFCAction() {
        return getParentActivity().nfcAction;
    }

    private void handleNFC() {
        // Show progress bar
        showProgress(true);
        setDataByNFC();

        // If geo location is verified register the place
        if (isActualLocation()) {

            // If network is available try to get additional place data from database and write to local database
            if (Support.isNetworkAvailable(getActivity())) {
                // Try to register place in database
                visitTag();
            } else {
                // Only write nfc place data to local database
                createPlace();
            }
        } else {
            // Can't verify the geo location
            Toast.makeText(getActivity(), Constants.ANNOUNCE_TAG_NOT_ACTUAL_PLACE, Toast.LENGTH_SHORT).show();
        }

        // Set nfc action to false;
        getParentActivity().nfcAction = false;
        // Hide progress bar
        showProgress(false);
    }

    // Set data for place by the NFC Message
    private void setDataByNFC() {
        mNewPlace = new Place();
        mNfcData = getArguments().getString(Constants.ARG_NFC_DATA);
        if (mNfcData != null) {
            String[] geo = mNfcData.split(",");
            // Set retrieved data for latitude, longitude and get datetime from class "Support"
            mNewPlace.setLatitude(Double.parseDouble(geo[0].split("geo:")[1]));
            mNewPlace.setLongitude(Double.parseDouble(geo[1]));
            mNewPlace.setDateTime(Support.getDateTime());
        }
    }

    // Create an "WebserviceManager" with method value "visitTag"
    // Try to insert a record into database by the data retrieved from the nfc scan (lat, lon, datTime)
    private void visitTag() {
        WebserviceManager webserviceManager = new WebserviceManager(getActivity(),
                Constants.WEBSERVICE_TASK_VISIT_TAG,
                mNewPlace.getLatitude(), mNewPlace.getLongitude(), mNewPlace.getDateTime());
        webserviceManager.doAccess();
    }

    // Create an "WebserviceManager" with method value "getPictureLink"
    // Try to fetch the picture for the passed parameter tagId which represents a place in the database
    private void getPictureLink(Integer tagId) {
        WebserviceManager manager = new WebserviceManager(getActivity(),
                Constants.WEBSERVICE_TASK_GET_PICTURE_LINK, tagId);
        manager.doAccess();
    }

    // Handles the database access of "visitTag" by a "WebserviceTaskResult"
    public void handleVisitTag(WebserviceTaskResult taskResult) {
        if (taskResult.getAppTagResult() != null) {
            int returnCode = taskResult.getAppTagResult().getReturnCode();

            // If the access was successfull update the table "place" and the table "user" by the retrieved place data
            // and try to get the related picture by tagId if it doesn't exist in the local database
            if (WebserviceManager.accessSuccessfull(returnCode) &&
                    taskResult.getAppTagResult().getTag() != null) {
                ContentValues values = setContentValues(taskResult.getAppTagResult().getTag());
                if (!mDbHandler.existsPicture(taskResult.getAppTagResult().getTag().getID())) {
                    getPictureLink(taskResult.getAppTagResult().getTag().getID());
                }
                Place newPlace = mDbHandler.createPlace(values);
                mDbHandler.updateUserByPlace(newPlace);
                mAdapter.add(newPlace);
            } else {
                // Write place to local database if the access wasn't successfull
                createPlace();
                WebserviceManager.toastErrorByReturnCode(getActivity(), returnCode);
                Toast.makeText(getActivity(), Constants.ERROR_WRITE_PLACE, Toast.LENGTH_SHORT).show();
            }
        } else {
            // Write place to local database if the access wasn't successfull
            createPlace();
            Toast.makeText(getActivity(), Constants.ERROR_WRITE_PLACE, Toast.LENGTH_SHORT).show();
        }

        mAdapter.notifyDataSetChanged();
    }

    // Handles the database access of "getPictureLink" by a "WebserviceTaskResult"
    public void handleGetPictureLink(WebserviceTaskResult taskResult) {
        // Insert the picture into local database if the result "byteArrayBuffer" is not null
        if (taskResult.getImageByteArrayBuffer() != null) {
            ContentValues values = new ContentValues();
            values.put(DatabaseHandler.IMAGE_COLUMN_TAG_ID, taskResult.getTagId());
            values.put(DatabaseHandler.IMAGE_COLUMN_IMAGE, taskResult.getImageByteArrayBuffer().toByteArray());
            mDbHandler.createImage(values);
        } else {
            Toast.makeText(getActivity(), Constants.ERROR_LOADING_IMAGE, Toast.LENGTH_SHORT).show();
        }
    }

    // Create record in table place
    private void createPlace() {
        ContentValues values = setContentValues(null);
        Place newPlace = mDbHandler.createPlace(values);
        mAdapter.add(newPlace);
    }

    // Checks if the nfc location is equal to the actual location retrieved by gps
    private boolean isActualLocation() {
        boolean ret = false;
        GPSManager gpsManager = new GPSManager(getActivity());
        if (gpsManager.setLocation()) {
            Double diffLat = getDistance(mNewPlace.getLatitude(), gpsManager.getLatitude());
            Double diffLong = getDistance(mNewPlace.getLongitude(), gpsManager.getLongitude());

            if (MAX_DIFF_MINUS <= diffLat && diffLat <= MAX_DIFF_PLUS &&
                    MAX_DIFF_MINUS <= diffLong && diffLong <= MAX_DIFF_PLUS) {
                ret = true;
            }
        } else {
            ret = false;
            Toast.makeText(getActivity(), Constants.ANNOUNCE_GPS_NOT_ACTIVATED, Toast.LENGTH_SHORT).show();
        }
        // TODO return ret;
        return true;
    }

    private Double getDistance(Double valueOne, Double valueTwo) {
        return valueOne - valueTwo;
    }

    // Sets the content values by the class "App_Tag"
    private ContentValues setContentValues(App_Tag appTag) {
        ContentValues values = new ContentValues();

        // If the database access was successfull put additional data to content values
        if (appTag != null) {
            values.put(DatabaseHandler.PLACE_COLUMN_TAG_ID, appTag.getID());
            values.put(DatabaseHandler.PLACE_COLUMN_PLACE, appTag.getName());
            values.put(DatabaseHandler.PLACE_COLUMN_POINTS, appTag.getScore());
            values.put(DatabaseHandler.PLACE_COLUMN_SYNCED, DatabaseHandler.VALUE_SYNCED);
        }
        // In every case put nfc data to content values
        values.put(DatabaseHandler.PLACE_COLUMN_LATITUDE, mNewPlace.getLatitude());
        values.put(DatabaseHandler.PLACE_COLUMN_LONGITUDE, mNewPlace.getLongitude());
        values.put(DatabaseHandler.PLACE_COLUMN_DATETIME, mNewPlace.getDateTime());
        values.put(DatabaseHandler.PLACE_COLUMN_USER_ID,  Support.userId);

        return values;
    }

    public void refresh() {
        // TODO refresh view
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
