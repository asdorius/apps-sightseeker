﻿#region usings

using System;

#endregion

namespace SightSeeker_Universal.Common
{
    internal class SightseekerServerException : Exception
    {
        public SightseekerServerException(int errorCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
        }

        public SightseekerServerException(int errorCode, string message, Exception inner)
            : base(message, inner)
        {
            ErrorCode = errorCode;
        }

        public int ErrorCode { get; private set; }
    }
}