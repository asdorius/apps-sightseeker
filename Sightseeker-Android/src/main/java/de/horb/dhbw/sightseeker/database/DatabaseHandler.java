package de.horb.dhbw.sightseeker.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import de.horb.dhbw.sightseeker.data.Image;
import de.horb.dhbw.sightseeker.data.Place;
import de.horb.dhbw.sightseeker.data.User;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.support.Support;
import de.horb.dhbw.sightseeker.webservice.App_Tag;
import de.horb.dhbw.sightseeker.webservice.App_User;

/**
 * Created by Simon on 09.04.2015.
 * This class is an interface to the local database
 */
public class DatabaseHandler extends SQLiteOpenHelper{

    public static final String TABLE_USER   = "user";
    public static final String TABLE_PLACE  = "place";
    public static final String TABLE_IMAGE  = "image";

    public static final String COLUMN_ID    = "_id";

    public static final String USER_COLUMN_USERNAME     = "user";
    public static final String USER_COLUMN_EMAIL        = "email";
    public static final String USER_COLUMN_PASSWORD     = "password";
    public static final String USER_COLUMN_COUNT_VISITED = "count_visited";
    public static final String USER_COLUMN_LAST_VISITED = "last_visited";
    public static final String USER_COLUMN_LAST_TAG_ID  = "last_tag_id";
    public static final String USER_COLUMN_TOTAL_POINTS = "total_points";
    public static final String USER_COLUMN_SYNCED       = "synced";

    public static final String PLACE_COLUMN_TAG_ID      = "tag_id";
    public static final String PLACE_COLUMN_PLACE       = "place";
    public static final String PLACE_COLUMN_POINTS      = "points";
    public static final String PLACE_COLUMN_LATITUDE    = "latitude";
    public static final String PLACE_COLUMN_LONGITUDE   = "longitude";
    public static final String PLACE_COLUMN_DATETIME    = "datetime";
    public static final String PLACE_COLUMN_USER_ID     = "user_id";
    public static final String PLACE_COLUMN_SYNCED      = "synced";

    public static final String IMAGE_COLUMN_TAG_ID      = "tag_id";
    public static final String IMAGE_COLUMN_IMAGE       = "image";

    public static final int DATABASE_BYTE_SIZE  = 8192;
    public static final int VALUE_SYNCED        = 1;
    public static final int VALUE_NOT_SYNCED    = 0;


    public static final String[] allColumnsUSER = {COLUMN_ID, USER_COLUMN_USERNAME,
            USER_COLUMN_EMAIL, USER_COLUMN_PASSWORD, USER_COLUMN_COUNT_VISITED,
            USER_COLUMN_LAST_VISITED, USER_COLUMN_LAST_TAG_ID, USER_COLUMN_TOTAL_POINTS, USER_COLUMN_SYNCED};

    public static final String[] allColumnsPlace = {COLUMN_ID, PLACE_COLUMN_TAG_ID,
            PLACE_COLUMN_PLACE, PLACE_COLUMN_POINTS, PLACE_COLUMN_LATITUDE,
            PLACE_COLUMN_LONGITUDE, PLACE_COLUMN_DATETIME, PLACE_COLUMN_USER_ID,
            PLACE_COLUMN_SYNCED};

    public static final String[] allColumnsImage = {COLUMN_ID, IMAGE_COLUMN_TAG_ID, IMAGE_COLUMN_IMAGE};

    // SQL statement for creating the table "user"
    private static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS " + TABLE_USER + " (" +
                                COLUMN_ID                   + " INTEGER PRIMARY KEY AUTOINCREMENT,"     +
                                USER_COLUMN_USERNAME        + " TEXT,"                                  +
                                USER_COLUMN_EMAIL           + " TEXT NOT NULL,"                         +
                                USER_COLUMN_PASSWORD        + " TEXT NOT NULL,"                         +
                                USER_COLUMN_COUNT_VISITED   + " TEXT,"                                  +
                                USER_COLUMN_LAST_VISITED    + " TEXT,"                                  +
                                USER_COLUMN_LAST_TAG_ID     + " TEXT,"                                  +
                                USER_COLUMN_TOTAL_POINTS    + " TEXT,"                                  +
                                USER_COLUMN_SYNCED          + " INTEGER DEFAULT 0"                      + ");";

    // SQL statement for creating the table "place"
    private static final String CREATE_TABLE_PLACE = "CREATE TABLE IF NOT EXISTS " + TABLE_PLACE + " (" +
                                COLUMN_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                                PLACE_COLUMN_TAG_ID     + " INTEGER DEFAULT 0,"                 +
                                PLACE_COLUMN_PLACE      + " TEXT,"                              +
                                PLACE_COLUMN_POINTS     + " INTEGER,"                           +
                                PLACE_COLUMN_LATITUDE   + " REAL NOT NULL,"                     +
                                PLACE_COLUMN_LONGITUDE  + " REAL NOT NULL,"                     +
                                PLACE_COLUMN_DATETIME   + " TEXT NOT NULL,"                     +
                                PLACE_COLUMN_USER_ID    + " INTEGER NOT NULL,"                  +
                                PLACE_COLUMN_SYNCED     + " INTEGER DEFAULT 0,"                 +
                                "FOREIGN KEY " + "(" + PLACE_COLUMN_USER_ID + ") "              +
                                "REFERENCES " + TABLE_USER + "(" + COLUMN_ID + ")"              + ");";

    // SQL statement for creating the table "image"
    private static final String CREATE_TABLE_IMAGE = "CREATE TABLE IF NOT EXISTS " + TABLE_IMAGE + " (" +
                                COLUMN_ID               + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                                IMAGE_COLUMN_TAG_ID     + " INTEGER UNIQUE NOT NULL,"                 +
                                IMAGE_COLUMN_IMAGE      + " BLOB NOT NULL,"                     +
                                "FOREIGN KEY " + "(" + IMAGE_COLUMN_TAG_ID + ") "               +
                                "REFERENCES " + TABLE_PLACE + "(" + PLACE_COLUMN_TAG_ID + ")"   + ");";

    // Local database
    private SQLiteDatabase mDatabase;

    public DatabaseHandler(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    // Calls the SQL statements for creating the tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_TABLE_PLACE);
        db.execSQL(CREATE_TABLE_IMAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Vorerst hoffentlich nicht notwendig
    }

    // Gets access to local database
    public void openDataBase() {
        mDatabase = getWritableDatabase();
    }

    // Returns all users registered in database
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();

        Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER, null, null, null, null, null);

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            User user = createUserByCursor(cursor);
            users.add(user);
            cursor.moveToPrevious();
        }

        cursor.close();
        return users;
    }

    // Returns the first user registered in database
    public User getFirstUser() {
        Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER,
                COLUMN_ID + " = " + 1, null, null, null, null);

        cursor.moveToFirst();

        User user = createUserByCursor(cursor);
        Support.userId = Long.valueOf(1);

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return user;
    }

    // Gets the user by the userId saved in the class "Support" at start-up
    public User getUser() {
        if (Support.userId != null) {
            Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER,
                    COLUMN_ID + " = " + Support.userId, null,
                    null, null, null);
            cursor.moveToFirst();
            User user = createUserByCursor(cursor);
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            return user;
        }
        return null;
    }

    public User getUserByEmail(String email) {
        Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER,
                USER_COLUMN_EMAIL + " = " + "'" + email + "'", null,
                null, null, null);

        cursor.moveToFirst();
        User user = createUserByCursor(cursor);
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return user;
    }

    // Returns true if a user exists with the passed email
    public boolean existsUser(String email) {
        boolean ret = false;

        Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER,
                USER_COLUMN_EMAIL + " = " + "'" + email + "'", null,
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            ret = true;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return ret;
    }

    private User createUserByCursor(Cursor cursor) {
        User user = new User();
        user.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        user.setUsername(cursor.getString(cursor.getColumnIndex(USER_COLUMN_USERNAME)));
        user.setEmail(cursor.getString(cursor.getColumnIndex(USER_COLUMN_EMAIL)));
        user.setPassword(cursor.getString(cursor.getColumnIndex(USER_COLUMN_PASSWORD)));
        user.setCountVisited(cursor.getInt(cursor.getColumnIndex(USER_COLUMN_COUNT_VISITED)));
        user.setLastVisited(cursor.getString(cursor.getColumnIndex(USER_COLUMN_LAST_VISITED)));
        user.setLastTagId(cursor.getInt(cursor.getColumnIndex(USER_COLUMN_LAST_TAG_ID)));
        user.setTotalPoints(cursor.getInt(cursor.getColumnIndex(USER_COLUMN_TOTAL_POINTS)));
        user.setSynced(cursor.getInt(cursor.getColumnIndex(USER_COLUMN_SYNCED)));
        return user;
    }

    // Creates a user in the database
    public Long createUser(ContentValues values) {
        Long insertId = mDatabase.insert(TABLE_USER, null, values);
        return insertId;
    }

    // Updates the user by the class "App_User" retrieved from a database access
    public void updateUser(App_User appUser) {
        ContentValues newValues = new ContentValues();

        // Update the username and set the attribute "synced" to 1 (true)
        newValues.put(USER_COLUMN_USERNAME, appUser.getUsername());
        newValues.put(USER_COLUMN_SYNCED, VALUE_SYNCED);

        mDatabase.update(TABLE_USER, newValues, COLUMN_ID + " = " + Support.userId, null);
    }

    // Updates the user by a place
    public void updateUserByPlace(Place newPlace) {
        Cursor cursor = mDatabase.query(TABLE_USER, allColumnsUSER,
                COLUMN_ID + " = " + Support.userId, null,
                null, null, null);
        User user = null;
        if (cursor.moveToFirst()) {
            user = createUserByCursor(cursor);
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        // Update the number of visited places
        // Update the last visited place
        // Update the last visited tag id
        // Update the total points for all visited places
        ContentValues newValues = new ContentValues();
        newValues.put(USER_COLUMN_COUNT_VISITED, user.getCountVisited()+1);
        newValues.put(USER_COLUMN_LAST_VISITED, newPlace.getPlace());
        newValues.put(USER_COLUMN_LAST_TAG_ID, newPlace.getTagId());
        newValues.put(USER_COLUMN_TOTAL_POINTS, user.getTotalPoints() + newPlace.getPoints());

        mDatabase.update(TABLE_USER, newValues, COLUMN_ID + " = " + Support.userId, null);
    }

    // Updates the user password by the passed parameter
    public void updateUserPassword(String newPassword) {
        ContentValues newValues = new ContentValues();
        newValues.put(USER_COLUMN_PASSWORD, newPassword);

        mDatabase.update(TABLE_USER, newValues, COLUMN_ID + " = " + Support.userId, null);
    }

    // Returns all places from local database
    public List<Place> getAllPlaces() {
        List<Place> places = new ArrayList<Place>();

        Cursor cursor = mDatabase.query(TABLE_PLACE, allColumnsPlace,
                PLACE_COLUMN_USER_ID + " = " + Support.userId, null, null, null, null);

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            Place place = createPlaceByCursor(cursor);
            places.add(place);
            cursor.moveToPrevious();
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return places;
    }

    // Returns the place relating to the passed tag id
    public Place getPlaceByTagId(Integer tagId) {
        Cursor cursor = mDatabase.query(TABLE_PLACE, allColumnsPlace,
                PLACE_COLUMN_USER_ID    + " = " + Support.userId + " AND " +
                PLACE_COLUMN_TAG_ID     + " = " + tagId, null, null, null, null);

        Place place = null;
        if (cursor.moveToFirst()) {
             place = createPlaceByCursor(cursor);
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return place;
    }

    // Returns all (unsynced) places where the attribute "synced" is still on 0
    public List<Place> getUnsyncedPlaces() {
        List<Place> places = new ArrayList<>();

        Cursor cursor = mDatabase.query(TABLE_PLACE, allColumnsPlace,
                PLACE_COLUMN_USER_ID + " = " + Support.userId + " AND " +
                PLACE_COLUMN_SYNCED + " = " + VALUE_NOT_SYNCED, null, null, null, null);

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            Place place = createPlaceByCursor(cursor);
            places.add(place);
            cursor.moveToPrevious();
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return places;
    }

    // Returns all tag id's having no relation to the table image
    public List<Integer> getNoImagePlaces() {
        List<Integer> tagIds = new ArrayList<>();
        String query = "SELECT DISTINCT " + PLACE_COLUMN_TAG_ID + " FROM " + TABLE_PLACE +
                        " WHERE " + PLACE_COLUMN_TAG_ID + " != " + 0 + " AND " + PLACE_COLUMN_TAG_ID
                        + " NOT IN (SELECT "+ IMAGE_COLUMN_TAG_ID + " FROM " + TABLE_IMAGE + ");";
        Cursor cursor = mDatabase.rawQuery(query, null);

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            tagIds.add(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_TAG_ID)));
            cursor.moveToPrevious();
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return tagIds;
    }

    // Returns all tag id's for the places
    public List<Integer> getAllTagIds() {
        List<Integer> tagIds = new ArrayList<>();
        String[] columnTagId = {PLACE_COLUMN_TAG_ID};

        Cursor cursor = mDatabase.query(TABLE_PLACE, columnTagId,
                PLACE_COLUMN_USER_ID + " = " + Support.userId + " AND " +
                        PLACE_COLUMN_TAG_ID + " != " + 0, null, null, null, null);

        cursor.moveToLast();
        while (!cursor.isBeforeFirst()) {
            tagIds.add(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_TAG_ID)));
            cursor.moveToPrevious();
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return tagIds;
    }

    // Updates a place by the class "App_Tag" retrieved from a database access
    public void updatePlaceByAppTag(App_Tag appTag) {
        ContentValues newValues = new ContentValues();
        // Update the tag id
        // Update the name for the place
        // Update the points for the place
        // Set attribute "synced" to 1 (true)
        newValues.put(PLACE_COLUMN_TAG_ID, appTag.getID());
        newValues.put(PLACE_COLUMN_PLACE, appTag.getName());
        newValues.put(PLACE_COLUMN_POINTS, appTag.getScore());
        newValues.put(PLACE_COLUMN_SYNCED, VALUE_SYNCED);

        mDatabase.update(TABLE_PLACE, newValues, PLACE_COLUMN_USER_ID + " = " + Support.userId + " AND " +
                                PLACE_COLUMN_DATETIME + " = " + "'" + appTag.getVisitingDate() + "'", null);
    }

    private Place createPlaceByCursor(Cursor cursor) {
        Place place = new Place();
        place.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        place.setTagId(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_TAG_ID)));
        place.setPlace(cursor.getString(cursor.getColumnIndex(PLACE_COLUMN_PLACE)));
        place.setPoints(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_POINTS)));
        place.setDateTime(cursor.getString(cursor.getColumnIndex(PLACE_COLUMN_DATETIME)));
        place.setLatitude(cursor.getDouble(cursor.getColumnIndex(PLACE_COLUMN_LATITUDE)));
        place.setLongitude(cursor.getDouble(cursor.getColumnIndex(PLACE_COLUMN_LONGITUDE)));
        place.setUserId(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_USER_ID)));
        place.setSynced(cursor.getInt(cursor.getColumnIndex(PLACE_COLUMN_SYNCED)));
        return place;
    }

    // Creates record in table place
    public Place createPlace(ContentValues values) {
        long insertId = mDatabase.insert(TABLE_PLACE, null, values);
        Cursor cursor = mDatabase.query(TABLE_PLACE, allColumnsPlace, COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Place place = createPlaceByCursor(cursor);
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return place;
    }

    // Creates record in table image
    public Image createImage(ContentValues values) {
        long insertId = mDatabase.insert(TABLE_IMAGE, null, values);
        Cursor cursor = mDatabase.query(TABLE_IMAGE, allColumnsImage, COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Image image = createImageByCursor(cursor);
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return image;
    }

    // Checks if a picture exists for the passed tag id
    public boolean existsPicture(Integer tagId) {
        boolean ret = false;

        Cursor cursor = mDatabase.query(TABLE_IMAGE, allColumnsImage,
                IMAGE_COLUMN_TAG_ID + " = " + tagId, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            ret = true;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return ret;
    }

    // Returns the image for the passed tag id
    public Image getImageByTagId(Integer tagId) {
        Cursor cursor = mDatabase.query(TABLE_IMAGE, allColumnsImage,
                IMAGE_COLUMN_TAG_ID + " = " + tagId, null, null, null, null);

        Image image = null;
        if (cursor.moveToFirst()) {
            image = createImageByCursor(cursor);
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return image;
    }

    private Image createImageByCursor(Cursor cursor) {
        Image image = new Image();
        image.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
        image.setTagId(cursor.getInt(cursor.getColumnIndex(IMAGE_COLUMN_TAG_ID)));
        image.setImageByteArray(cursor.getBlob(cursor.getColumnIndex(IMAGE_COLUMN_IMAGE)));
        return image;
    }
}
