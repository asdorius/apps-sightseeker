package de.horb.dhbw.sightseeker.webservice;

import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

/**
 * Created by robin on 17.05.2015.
 */
public class MarshalDoubleSingleton implements Marshal {
    private static MarshalDoubleSingleton ourInstance = new MarshalDoubleSingleton();

    public static MarshalDoubleSingleton getInstance() {
        return ourInstance;
    }

    private MarshalDoubleSingleton() {
    }

    public static void init(SoapSerializationEnvelope cm) {
        cm.implicitTypes = true;
        cm.encodingStyle = SoapSerializationEnvelope.XSD;
        getInstance().register(cm);
    }

    public Object readInstance(XmlPullParser parser, String namespace, String name,
                               PropertyInfo expected) throws IOException, XmlPullParserException {
        return Double.parseDouble(parser.nextText());
    }

    public void register(SoapSerializationEnvelope cm) {
        cm.addMapping(cm.xsd, "double", Double.class, this);
    }

    public void writeInstance(XmlSerializer writer, Object obj) throws IOException {
        writer.text(obj.toString());
    }
}
