package de.horb.dhbw.sightseeker.webservice;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public final class SightSeekerServicesPort {

private Object processClass(Class<?> _cls, Object value) {
        Object _obj = null;
        try {
            int _len = ((SoapObject) value).getPropertyCount();
            if (_cls.isArray()) {
                Class<?> _cmp = _cls.getComponentType();
                _obj = Array.newInstance(_cmp, _len);
                for (int _i = 0; _i < _len; _i++) {
                    Object _elem = ((SoapObject) value).getProperty(_i);
                    if (_elem instanceof SoapObject) {
                        ((Object[])_obj)[_i] = processClass(_cmp, _elem);
                    } else {
                        ((Object[])_obj)[_i] = ((SoapObject) value).getProperty(_i);
                    }
                }
            } else {
                Constructor<?> _ctr = _cls.getConstructor();
                _obj = _ctr.newInstance();
                for (int _i = 0; _i < _len; _i++) {
                    Object _prop = ((SoapObject) value).getProperty(_i);
                    if (_prop instanceof SoapObject) {
                        Class<?> _type = _cls.getDeclaredFields()[_i].getType();
                        ((SoapObject) _obj).setProperty(_i, processClass(_type, _prop));
                    } else {
                        ((SoapObject) _obj).setProperty(_i, ((SoapObject) value).getProperty(_i));
                    }
                }
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return _obj;
    }    
	
	public  de.horb.dhbw.sightseeker.webservice.App_User_Result getUserFromToken(java.lang.String token) throws Exception {
        SoapObject _client = new SoapObject("", "getUserFromToken");
        _client.addProperty("token", token);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.App_User_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.App_User_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.String_Result login(java.lang.String email, java.lang.String pass) throws Exception {
        SoapObject _client = new SoapObject("", "login");
        _client.addProperty("email", email);
        _client.addProperty("pass", pass);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.String_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.String_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.ReturnCode_Result logout(java.lang.String token) throws Exception {
        SoapObject _client = new SoapObject("", "logout");
        _client.addProperty("token", token);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.ReturnCode_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.ReturnCode_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.ReturnCode_Result registerUser(java.lang.String email, java.lang.String username, java.lang.String password) throws Exception {
        SoapObject _client = new SoapObject("", "registerUser");
        _client.addProperty("email", email);
        _client.addProperty("username", username);
        _client.addProperty("password", password);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.ReturnCode_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.ReturnCode_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.App_Tag_Result visitTag(java.lang.String token, double longitude, double latitude, java.lang.String visitingDate) throws Exception {
        SoapObject _client = new SoapObject("", "visitTag");
        _client.addProperty("token", token);
        _client.addProperty("longitude", longitude + "");
        _client.addProperty("latitude", latitude + "");
        _client.addProperty("visitingDate", visitingDate);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.App_Tag_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.App_Tag_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.App_UserTags_Result getNearTags(java.lang.String token, double _long, double lat, int offset, int limit, int dist) throws Exception {
        SoapObject _client = new SoapObject("", "getNearTags");
        _client.addProperty("token", token);
        _client.addProperty("_long", _long + "");
        _client.addProperty("lat", lat + "");
        _client.addProperty("offset", offset + "");
        _client.addProperty("limit", limit + "");
        _client.addProperty("dist", dist + "");
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.App_UserTags_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.App_UserTags_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.ReturnCode_Result updateUserPassword(java.lang.String token, java.lang.String newPass) throws Exception {
        SoapObject _client = new SoapObject("", "updateUserPassword");
        _client.addProperty("token", token);
        _client.addProperty("newPass", newPass);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.ReturnCode_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.ReturnCode_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.App_UserTags_Result getVisitedTagsFromToken(java.lang.String token, int offset, int limit) throws Exception {
        SoapObject _client = new SoapObject("", "getVisitedTagsFromToken");
        _client.addProperty("token", token);
        _client.addProperty("offset", offset + "");
        _client.addProperty("limit", limit + "");
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.App_UserTags_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.App_UserTags_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.Int_Result countVisitedTagsFromToken(java.lang.String token) throws Exception {
        SoapObject _client = new SoapObject("", "countVisitedTagsFromToken");
        _client.addProperty("token", token);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.Int_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.Int_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.String_Result getInfoTextFromTagID(java.lang.String token, int tagID) throws Exception {
        SoapObject _client = new SoapObject("", "getInfoTextFromTagID");
        _client.addProperty("token", token);
        _client.addProperty("tagID", tagID + "");
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.String_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.String_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.String_Result getPictureLinkFromTagID(java.lang.String token, int tagID) throws Exception {
        SoapObject _client = new SoapObject("", "getPictureLinkFromTagID");
        _client.addProperty("token", token);
        _client.addProperty("tagID", tagID + "");
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.String_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.String_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.Int_Result getUserScoreFromToken(java.lang.String token) throws Exception {
        SoapObject _client = new SoapObject("", "getUserScoreFromToken");
        _client.addProperty("token", token);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.Int_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.Int_Result) _returned;
    }


    public  de.horb.dhbw.sightseeker.webservice.App_Highscore_Result getGlobalHighscore(java.lang.String token) throws Exception {
        SoapObject _client = new SoapObject("", "getGlobalHighscore");
        _client.addProperty("token", token);
        SoapSerializationEnvelope _envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        MarshalDoubleSingleton.init(_envelope);
        _envelope.bodyOut = _client;
        HttpTransportSE _ht = new HttpTransportSE(Configuration.getWsUrl());
        _ht.call("", _envelope);
        SoapObject _ret = (SoapObject) _envelope.getResponse();
        Object _returned = processClass(de.horb.dhbw.sightseeker.webservice.App_Highscore_Result.class, _ret);
        return (de.horb.dhbw.sightseeker.webservice.App_Highscore_Result) _returned;
    }


}
