﻿#region usings

using System;
using System.Globalization;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Networking.Proximity;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using NdefLibrary.Ndef;
using SightSeeker_Universal.Common;

#endregion

namespace SightSeeker_Universal.Specific_Logic
{
    internal class NFCReader
    {
        private readonly ProximityDevice device = ProximityDevice.GetDefault();

        public NFCReader()
        {
            device.SubscribeForMessage("NDEF", Read);
        }

        public event EventHandler TagVisited;

        public async void Read(ProximityDevice sender, ProximityMessage message)
        {
            // Parse raw byte array to NDEF message
            var rawMsg = message.Data.ToArray();
            var ndefMessage = NdefMessage.FromByteArray(rawMsg);
            AppTag possibleAppTag = new AppTag();
            // Loop over all records contained in the NDEF message
            foreach (NdefRecord record in ndefMessage)
            {
// Go through each record, check if it's a Smart Poster
                if (record.CheckSpecializedType(false) == typeof (NdefSpRecord))
                {
                    // Convert and extract Smart Poster info
                    var spRecord = new NdefSpRecord(record);
                    if (spRecord.Uri.StartsWith("geo:"))
                    {
                        try
                        {
                            string[] geoData = spRecord.Uri.Substring(4).Split(',');
                            possibleAppTag.Longitude = double.Parse(geoData[1], CultureInfo.InvariantCulture);
                            possibleAppTag.Latitude = double.Parse(geoData[0], CultureInfo.InvariantCulture);
                            possibleAppTag.Name = Uri.UnescapeDataString(spRecord.Titles[0].Text);
                            Backend.VisitTag((Application.Current as App).LoggedInUser, possibleAppTag);
                        }
                        catch
                        {
                            return;
                        }
                        EventHandler handler = TagVisited;
                        if (handler != null)
                        {
                            handler(this, EventArgs.Empty);
                        }
                    }
                }
            }
        }
    }
}