package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class App_Score extends SoapObject {
    private java.lang.String username;

    private java.lang.Integer score;

    public App_Score() {
        super("", "");
    }
    public void setUsername(java.lang.String username) {
        this.username = username;
    }

    public java.lang.String getUsername() {
        return this.username;
    }

    public void setScore(java.lang.Integer score) {
        this.score = score;
    }

    public java.lang.Integer getScore() {
        return this.score;
    }

    public int getPropertyCount() {
        return 2;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return username;
        case 1: return score;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: username = (java.lang.String) __obj; break;
        case 1: score = (java.lang.Integer) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "username";
            __info.type = java.lang.String.class; break;
        case 1:
            __info.name = "score";
            __info.type = java.lang.Integer.class; break;
        }
    }

}
