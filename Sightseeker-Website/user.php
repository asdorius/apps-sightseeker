<?php
	require_once 'Zend/Soap/Client.php';
	session_start();

  	if(!isset($_SESSION['logedIn']) || !isset($_SESSION['loginToken']))
	{
		header("Redirect to login Page");
   		header("Location: login.php");
    	exit;
	}
		
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Sightseeker - User bearbeiten</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">SightSeeker</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li><a href="index.php">Home</a></li>
                  <li class="active"><a href="user.php">User</a></li>
                  <li><a href="places.php">Meine Orte</a></li>
                  <li><a href="map.php">Karte</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <form method="post" action="user.php" class="form-horizontal">
				<fieldset>

				<!-- Form Name -->
				<h3>User bearbeiten</h3>

				<?php
					if (isset($_SESSION['passwordChanged']))
					{
                        unset($_SESSION['passwordChanged']);
						echo "<h3>Passwort erfolgreich geändert</h3>";
					}
					if (isset($_SESSION['usernameChanged']))
					{
                        unset($_SESSION['usernameChanged']);
						echo "<h3>Der Username erfolgreich geändert</h3>";
					}
                    if (isset($_SESSION['passwordChangeFailed']))
					{
                        unset($_SESSION['passwordChangeFailed']);
						echo "<h3>Das Passwort konnte nicht geändert werden</h3>";
					}
					if (isset($_SESSION['usernameChangeFailed']))
					{
                        unset($_SESSION['usernameChangeFailed']);
						echo "<h3>Der Username konnte nicht geändert werden</h3>";
					}
				?>
				<!-- Password input-->
				<div class="form-group">
  					<label class="col-md-4 control-label" for="pass">Passwort</label>
  					<div class="col-md-4">
    					<input id="pass" name="pass" placeholder="123456789" class="form-control input-md" required="" type="password" x-moz-errormessage="Bitte geben Sie ein gültiges Passwort ein.">
    
  					</div>
				</div>

				<!-- Button (Double) -->
				<div class="form-group">
  					<label class="col-md-4 control-label" for="savebutton"></label>
  					<div class="col-md-4">
    					<button id="savebutton" name="savebutton" class="btn btn-primary">Speichern</button>
    					<!--<button id="abortbutton" name="abortbutton" class="btn btn-danger">Abbrechen</button> -->
  					</div>
				</div>

				</fieldset>
			</form>
          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>&copy; SightSeeker-App 2015 DHBW-Horb.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
<?php
 	function updateUserPassword()
	{
        $client = new Zend_Soap_Client("http://localhost/sightseeker/server.php?wsdl");

    	$c = $client->updateUserPassword($_SESSION["loginToken"], $_POST["pass"]);
    	$rC = $c -> returnCode;
    	// Wenn User eingeloggt
    	if ($rC == 0) 
    	{
    		$_SESSION['passwordChanged'] = 'true';

    		header("Redirect to user Page");
    		header("Location: user.php");
    		exit;
    	}
    	// Wenn Fehler aufgetreten oder Benutzerdaten falsch
   		else
    	{
        	$_SESSION['passwordChangeFailed'] = 'false';

        	header("Redirect to user Page");
    		header("Location: user.php");
    		exit;
    	}
	}
	if(isset($_POST['savebutton']))
	{
   		updateUserPassword();
	} 

?>