﻿#region usings

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.Devices.Geolocation;
using Windows.Security.Cryptography.Certificates;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using SightSeeker_Universal.DataModel;

#endregion

namespace SightSeeker_Universal.Common
{
    /// <summary>
    ///     Backend manages all communication with the SightSeeker server(s).
    ///     It provides all important functionality for interaction with the service.
    /// </summary>
    public static class Backend
    {
        private static readonly HttpClient Client;

        private static readonly Uri SightSeekerUrl =
            new Uri("https://star-i13013.ba-horb.de:23443/sightseeker/server.php");

        /// <summary>
        ///     Static construction ensures that we initialize the HttpClient right,
        ///     ignoring all certiicate errors.
        /// </summary>
        static Backend()
        {
            var filter = new HttpBaseProtocolFilter();
            filter.IgnorableServerCertificateErrors.Add(ChainValidationResult.Untrusted);

            Client = new HttpClient(filter);
        }

        public static async Task<User> LoginAndGetSavedUser()
        {
            StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("UserConfig.xml");
            string data = await FileIO.ReadTextAsync(file);
            XDocument userConfig = XDocument.Parse(data);
            string email = userConfig.Root.Element(XName.Get("Email")).Value;
            string password = userConfig.Root.Element(XName.Get("Password")).Value;
            return await LoginAndGetUser(email, password);
        }

        public static async Task<User> LoginAndGetUser(string email, string password)
        {
            string requestString = BackendRequests.LoginRequest(email, password);
            HttpRequestMessage loginRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage loginResponse = await Client.SendRequestAsync(loginRequest);

            string userToken = await BackendResponseDecoder.DecodeLoginResponse(loginResponse);

            await SaveUserConfig(email, password);
            return new User(email, email, userToken, password);
        }

        private static async Task SaveUserConfig(string email, string password)
        {
            StorageFile file =
                await
                    ApplicationData.Current.LocalFolder.CreateFileAsync("UserConfig.xml",
                        CreationCollisionOption.ReplaceExisting);
            XDocument document = new XDocument();
            XElement emailElement = new XElement(XName.Get("Email"));
            emailElement.Value = email;
            XElement passElement = new XElement(XName.Get("Password"));
            passElement.Value = password;
            document.Add(new XElement(XName.Get("User")));
            document.Root.Add(emailElement);
            document.Root.Add(passElement);
            FileIO.WriteTextAsync(file, document.ToString());
        }

        public static async Task<bool> LogoutUser(User user)
        {
            string requestString = BackendRequests.LogoutRequest(user);
            HttpRequestMessage logoutRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage logoutResponse = await Client.SendRequestAsync(logoutRequest);

            await RemoveUserConfig();
            BackendResponseDecoder.DecodeLogoutResponse(logoutResponse);
            return true;
        }

        private static async Task RemoveUserConfig()
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("UserConfig.xml",
                    CreationCollisionOption.ReplaceExisting);
                file.DeleteAsync();
            }
            catch { }
        }

        public static async Task<string> GetInfoTextFromTag(User user, AppTag tag)
        {
            string requestString = BackendRequests.GetInfoTextFromTagRequest(user, tag);
            HttpRequestMessage getInfoTextFromTagRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage getInfoTextFromTagResponse = await Client.SendRequestAsync(getInfoTextFromTagRequest);

            string result = await BackendResponseDecoder.DecodeGetInfoTextFromTagResponse(getInfoTextFromTagResponse);

            return result;
        }

        public static async Task<string> GetPictureLinkFromTag(User user, AppTag tag)
        {
            string requestString = BackendRequests.GetPictureLinkFromTagRequest(user, tag);
            HttpRequestMessage getPictureLinkFromTagRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage getPictureLinkFromTagResponse =
                await Client.SendRequestAsync(getPictureLinkFromTagRequest);

            string result =
                await BackendResponseDecoder.DecodeGetPictureLinkFromTagResponse(getPictureLinkFromTagResponse);

            return result;
        }

        public static async Task<int> GetUserScoreFromToken(User user)
        {
            string requestString = BackendRequests.GetUserScoreFromTokenRequest(user);
            HttpRequestMessage getUserScoreFromTokenRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage getUserScoreFromTokenResponse =
                await Client.SendRequestAsync(getUserScoreFromTokenRequest);

            string result =
                await BackendResponseDecoder.DecodeGetUserScoreFromTokenResponse(getUserScoreFromTokenResponse);

            return int.Parse(result);
        }

        public static async Task<List<UserScore>> GetGlobalHighscoreFromToken(User user)
        {
            string requestString = BackendRequests.GetGlobalHighscoreFromTokenRequest(user);
            HttpRequestMessage getGlobalHighscoreFromTokenRequest = new HttpRequestMessage(HttpMethod.Get,
                SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage getGlobalHighscoreFromTokenResponse =
                await Client.SendRequestAsync(getGlobalHighscoreFromTokenRequest);

            List<UserScore> result =
                await
                    BackendResponseDecoder.DecodeGetGlobalHighscoreFromTokenResponse(getGlobalHighscoreFromTokenResponse);

            return result;
        }

        public static async void RegisterUser(string email, string password)
        {
            string requestString = BackendRequests.RegisterUserRequest(email, email, password);
            HttpRequestMessage registerUserRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage registerUserResponse = await Client.SendRequestAsync(registerUserRequest);

            BackendResponseDecoder.DecodeRegisterUserResponse(registerUserResponse);
        }

        public static async void UpdatePassword(User user, string password)
        {
            string requestString = BackendRequests.UpdatePasswordRequest(user, password);
            HttpRequestMessage updatePasswordRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage updatePasswordResponse = await Client.SendRequestAsync(updatePasswordRequest);

            BackendResponseDecoder.DecodeUpdatePasswordResponse(updatePasswordResponse);
        }

        public static async Task<int> CountVisitedTagsFromUser(User user)
        {
            string requestString = BackendRequests.CountVisitedTagsRequest(user);
            HttpRequestMessage countVisitedTagsRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            HttpResponseMessage countVisitedTagsResponse = await Client.SendRequestAsync(countVisitedTagsRequest);

            return await BackendResponseDecoder.DecodeCountVisitedTagsResponse(countVisitedTagsResponse);
        }

        // visit tag hat als optionales feld jez visit date
        public static async void VisitTag(User user, AppTag tag)
        {
            //double longitude = 0;
            //double latitude = 0;
            //Geolocator geolocator = new Geolocator();
            //geolocator.DesiredAccuracyInMeters = 10;

            //try
            //{
            //    Geoposition geoposition = await geolocator.GetGeopositionAsync();
            //    if (distFrom(
            //        geoposition.Coordinate.Latitude,
            //        geoposition.Coordinate.Longitude,
            //        tag.Latitude,
            //        tag.Longitude) > 40)
            //    {
            //        throw new NotNearTagException();
            //    }

            //    longitude = geoposition.Coordinate.Point.Position.Longitude;
            //    latitude = geoposition.Coordinate.Point.Position.Latitude;
            //}
            //catch (NotNearTagException)
            //{
            //    new MessageDialog("Your location doesn't match the tag you have scanned.", "Invalid Scan").ShowAsync();
            //    return;
            //}
            //catch (Exception ex)
            //{
            //    if ((uint)ex.HResult == 0x80004004)
            //    {
            //        // the application does not have the right capability or the location master switch is off
            //        new MessageDialog("Location not enabled in phone settings", "Location").ShowAsync();

            //    }
            //    return;
            //}

            string currentTime = XmlConvert.ToString(DateTime.Now, "yyyy-MM-ddTHH:mm:sszz:00");

            string requestString = BackendRequests.VisitTagRequest(user, tag.Longitude, tag.Latitude, currentTime);
            HttpRequestMessage visitTagRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            try
            {
                HttpResponseMessage visitTagResponse = await Client.SendRequestAsync(visitTagRequest);
                BackendResponseDecoder.DecodeVisitTagResponse(visitTagResponse);
            }
            catch (InvalidKoordinatesException)
            {
                new MessageDialog("Wrong Koordinates", "Error").ShowAsync();
            }
            catch (Exception exc)
            {
                StoreRequest(requestString);
            }
        }

        public static async Task<List<AppTag>> GetNearTags(User user, int offset, int limit, int distance)
        {
            double longitude = 0;
            double latitude = 0;
            Geolocator geolocator = new Geolocator();
            geolocator.DesiredAccuracyInMeters = 50;
            List<AppTag> result = new List<AppTag>();

            try
            {
                Geoposition geoposition =
                    await geolocator.GetGeopositionAsync(TimeSpan.FromMinutes(2), TimeSpan.FromSeconds(10)
                        );

                longitude = geoposition.Coordinate.Point.Position.Longitude;
                latitude = geoposition.Coordinate.Point.Position.Latitude;
            }

            catch (Exception ex)
            {
                if ((uint) ex.HResult == 0x80004004)
                {
                    // the application does not have the right capability or the location master switch is off
                    new MessageDialog("Location not enabled in phone settings", "Location").ShowAsync();
                }
                return result; //TODO: ?
            }

            string requestString = BackendRequests.GetNearTagsRequest(user, longitude, latitude, offset, limit, distance);
            HttpRequestMessage getNearTagRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl)
            {
                Content = new HttpStringContent(requestString)
            };

            try
            {
                HttpResponseMessage getNearTagsResponse = await Client.SendRequestAsync(getNearTagRequest);
                string responseContent = responseContent = await getNearTagsResponse.Content.ReadAsStringAsync();
                result = await BackendResponseDecoder.DecodeGetNearTagsResponse(responseContent);
                return result;
            }
            catch (Exception exc)
            {
                return result;
            }
        }

        public static async Task<List<AppTag>> GetVisitedTagsFromUser(User user, int offset, int limit)
        {
            string requestString = BackendRequests.GetVisitedTagsFromTokenRequest(user, offset, limit);
            HttpRequestMessage getVisitedTagsFromTokenRequest = new HttpRequestMessage(HttpMethod.Get, SightSeekerUrl);
            getVisitedTagsFromTokenRequest.Content = new HttpStringContent(requestString);

            string responseContent = "";
            bool localStorageMustBeConsulted = false;
            try
            {
                HttpResponseMessage getVisitedTagsFromTokenResponse =
                    await Client.SendRequestAsync(getVisitedTagsFromTokenRequest);
                responseContent = await getVisitedTagsFromTokenResponse.Content.ReadAsStringAsync();

                StoreVisitedTagsInLocalStore(responseContent);
            }
            catch (Exception)
            {
                localStorageMustBeConsulted = true;
            }
            if (localStorageMustBeConsulted)
            {
                responseContent = await GetVisitedTagsFromLocalStore();
            }
            List<AppTag> appTagList = null;

            appTagList =
                await BackendResponseDecoder.DecodeGetVisitedTagsFromTokenResponse(responseContent);

            return appTagList;
        }

        private static async void StoreNearTagsInLocalStore(string content)
        {
            XElement xResponse = XElement.Parse(content);
            StorageFile file =
                await
                    ApplicationData.Current.LocalFolder.CreateFileAsync("NearTags.xml",
                        CreationCollisionOption.OpenIfExists);
            var responseStore = new XDocument();
            responseStore.Root.Add(xResponse);
            await FileIO.WriteTextAsync(file, responseStore.ToString());
        }

        private static async Task<string> GetNearTagsFromLocalStore()
        {
            StorageFile file =
                await
                    ApplicationData.Current.LocalFolder.CreateFileAsync("NearTags.xml",
                        CreationCollisionOption.OpenIfExists);
            string data = await FileIO.ReadTextAsync(file);
            XDocument visitedTagsStore = XDocument.Parse(data);
            StringBuilder builder = new StringBuilder();
            foreach (XElement element in visitedTagsStore.Root.Elements())
            {
                builder.Append(element);
            }
            return builder.ToString();
        }

        private static async void StoreVisitedTagsInLocalStore(string content)
        {
            XElement xRequest = XElement.Parse(content);
            StorageFile file =
                await
                    ApplicationData.Current.LocalFolder.CreateFileAsync("VisitedTags.xml",
                        CreationCollisionOption.OpenIfExists);
            var requestStore = new XDocument();
            requestStore.Add(new XElement(XName.Get("VisitedTags")));
            requestStore.Root.Add(xRequest);
            await FileIO.WriteTextAsync(file, requestStore.ToString());
        }

        private static async Task<string> GetVisitedTagsFromLocalStore()
        {
            try
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync("RequestStore.xml");
                string data = await FileIO.ReadTextAsync(file);
                XDocument visitedTagsStore = XDocument.Parse(data);
                StringBuilder builder = new StringBuilder();
                foreach (XElement element in visitedTagsStore.Root.Elements())
                {
                    builder.Append(element);
                }
                return builder.ToString();
            }
            catch
            {
                return "";
            }
        }

        private static async void StoreRequest(string request)
        {
            XElement xRequest = XElement.Parse(request);
            StorageFile file =
                await
                    ApplicationData.Current.LocalFolder.CreateFileAsync("RequestStore.xml",
                        CreationCollisionOption.OpenIfExists);
            string data = await FileIO.ReadTextAsync(file);
            XDocument requestStore = data == string.Empty ? XDocument.Load("RequestStore.xml") : XDocument.Parse(data);
            requestStore.Root.Add(xRequest);
            await FileIO.WriteTextAsync(file, requestStore.ToString());
        }

        private static double distFrom(double lat1, double lng1, double lat2, double lng2)
        {
            double earthRadius = 6371000; //meters
            double dLat = ToRadians(lat2 - lat1);
            double dLng = ToRadians(lng2 - lng1);
            double a = Math.Sin(dLat/2)*Math.Sin(dLat/2) +
                       Math.Cos(ToRadians(lat1))*Math.Cos(ToRadians(lat2))*
                       Math.Sin(dLng/2)*Math.Sin(dLng/2);
            double c = 2*Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            float dist = (float) (earthRadius*c);

            return dist;
        }

        public static double ToRadians(double angle)
        {
            return (Math.PI/180)*angle;
        }
    }
}