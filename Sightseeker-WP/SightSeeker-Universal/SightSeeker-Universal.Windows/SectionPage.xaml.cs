﻿#region usings

using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using SightSeeker_Universal.Common;

#endregion

namespace SightSeeker_Universal
{
    /// <summary>
    ///     Eine Seite, auf der eine Übersicht über eine einzelne Gruppe einschließlich einer Vorschau der Elemente
    ///     in der Gruppe angezeigt wird.
    /// </summary>
    public sealed partial class SectionPage : Page
    {
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();

        public SectionPage()
        {
            InitializeComponent();
            NavigationHelper = new NavigationHelper(this);
            NavigationHelper.LoadState += NavigationHelper_LoadState;
        }

        /// <summary>
        ///     Ruft den NavigationHelper ab, der zur Unterstützung bei der Navigation und Verwaltung der Prozesslebensdauer
        ///     verwendet wird.
        /// </summary>
        public NavigationHelper NavigationHelper { get; private set; }

        /// <summary>
        ///     Ruft das DefaultViewModel ab. Dies kann in ein stark typisiertes Anzeigemodell geändert werden.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return defaultViewModel; }
        }

        /// <summary>
        ///     Füllt die Seite mit Inhalt auf, der bei der Navigation übergeben wird.  Gespeicherte Zustände werden ebenfalls
        ///     bereitgestellt, wenn eine Seite aus einer vorherigen Sitzung neu erstellt wird.
        /// </summary>
        /// <param name="sender">
        ///     Die Quelle des Ereignisses, normalerweise <see cref="NavigationHelper" />
        /// </param>
        /// <param name="e">
        ///     Ereignisdaten, die die Navigationsparameter bereitstellen, die an
        ///     <see cref="Frame.Navigate(Type, object)" /> als diese Seite ursprünglich angefordert wurde und
        ///     ein Wörterbuch des Zustands, der von dieser Seite während einer früheren
        ///     beibehalten wurde.  Der Zustand ist beim ersten Aufrufen einer Seite NULL.
        /// </param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            // TODO: Ein geeignetes Datenmodell für die problematische Domäne erstellen, um die Beispieldaten auszutauschen
            var group = await SampleDataSource.GetGroupAsync((string) e.NavigationParameter);
            DefaultViewModel["Group"] = group;
            DefaultViewModel["Items"] = group.Items;
        }

        /// <summary>
        ///     Wird aufgerufen, wenn auf ein Element geklickt wird.
        /// </summary>
        /// <param name="sender">Die GridView, die das Element anzeigt, auf das geklickt wurde.</param>
        /// <param name="e">Ereignisdaten, die das angeklickte Element beschreiben.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Zur entsprechenden Zielseite navigieren und die neue Seite konfigurieren,
            // indem die erforderlichen Informationen als Navigationsparameter übergeben werden
            var itemId = ((SampleDataItem) e.ClickedItem).UniqueId;
            Frame.Navigate(typeof (ItemPage), itemId);
        }

        #region NavigationHelper-Registrierung

        /// <summary>
        ///     Die in diesem Abschnitt bereitgestellten Methoden werden einfach verwendet, um
        ///     damit NavigationHelper auf die Navigationsmethoden der Seite reagieren kann.
        ///     Platzieren Sie seitenspezifische Logik in Ereignishandlern für
        ///     <see cref="Common.NavigationHelper.LoadState" />
        ///     and <see cref="Common.NavigationHelper.SaveState" />.
        ///     Der Navigationsparameter ist in der LoadState-Methode verfügbar
        ///     zusätzlich zum Seitenzustand, der während einer früheren Sitzung beibehalten wurde.
        /// </summary>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            NavigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            NavigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}