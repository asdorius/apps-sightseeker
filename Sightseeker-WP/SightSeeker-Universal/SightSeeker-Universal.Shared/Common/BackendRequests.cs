﻿#region usings

using System.Globalization;
using System.Xml.Linq;
using SightSeeker_Universal.DataModel;

#endregion

namespace SightSeeker_Universal.Common
{
    internal static class BackendRequests
    {
        private static readonly string _soapEnvNS = "http://schemas.xmlsoap.org/soap/envelope/";
        private static readonly string _sightseekerNS = "https://star-i13013.ba-horb.de:23443/sightseeker/server.php";

        static BackendRequests()
        {
            _soapEnvNS = "http://schemas.xmlsoap.org/soap/envelope/";
            _sightseekerNS = "https://star-i13013.ba-horb.de:23443/sightseeker/server.php";
        }

        internal static string LoginRequest(string email, string password)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/LoginTemplate.xml");
            XElement emailField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("login", _sightseekerNS))
                .Element(XName.Get("email"));

            XElement passField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("login", _sightseekerNS))
                .Element(XName.Get("pass"));

            emailField.Value = email;
            passField.Value = password;

            return document.ToString();
        }

        internal static string LogoutRequest(User user)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/LogoutTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("logout", _sightseekerNS))
                .Element(XName.Get("token"));

            tokenField.Value = user.Token;

            return document.ToString();
        }

        internal static string RegisterUserRequest(string email, string name, string password)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/RegisterUserTemplate.xml");
            XElement emailField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("registerUser", _sightseekerNS))
                .Element(XName.Get("email"));
            XElement nameField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("registerUser", _sightseekerNS))
                .Element(XName.Get("username"));
            XElement passwordField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("registerUser", _sightseekerNS))
                .Element(XName.Get("password"));

            emailField.Value = email;
            nameField.Value = name;
            passwordField.Value = password;

            return document.ToString();
        }

        internal static string GetInfoTextFromTagRequest(User user, AppTag tag)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/GetInfoTextFromTagIdTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getInfoTextFromTagID", _sightseekerNS))
                .Element(XName.Get("token"));
            XElement tagIdField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getInfoTextFromTagID", _sightseekerNS))
                .Element(XName.Get("tagID"));

            tokenField.Value = user.Token;
            tagIdField.Value = tag.ID;


            return document.ToString();
        }

        internal static string GetPictureLinkFromTagRequest(User user, AppTag tag)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/GetPictureLinkFromTagIdTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getPictureLinkFromTagID", _sightseekerNS))
                .Element(XName.Get("token"));
            XElement tagIdField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getPictureLinkFromTagID", _sightseekerNS))
                .Element(XName.Get("tagID"));

            tokenField.Value = user.Token;
            tagIdField.Value = tag.ID;


            return document.ToString();
        }

        internal static string GetUserScoreFromTokenRequest(User user)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/GetUserScoreFromTokenTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getUserScoreFromToken", _sightseekerNS))
                .Element(XName.Get("token"));


            tokenField.Value = user.Token;

            return document.ToString();
        }

        internal static string GetGlobalHighscoreFromTokenRequest(User user)
        {
            XDocument document =
                XDocument.Load(@"Assets/BackendRequestTemplates/GetGlobalHighscoreFromTokenTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getHighscoreFromToken", _sightseekerNS))
                .Element(XName.Get("token"));


            tokenField.Value = user.Token;

            return document.ToString();
        }

        internal static string GetNearTagsRequest(User user, double longitude, double latitude, int offset, int limit,
            int distance)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/GetNearTagsTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("token"));
            XElement longField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("long"));
            XElement latField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("lat"));
            XElement offsetField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("offset"));
            XElement limitField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("limit"));
            XElement distanceField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getNearTags", _sightseekerNS))
                .Element(XName.Get("dist"));


            tokenField.Value = user.Token;
            longField.Value = longitude.ToString(CultureInfo.InvariantCulture);
            latField.Value = latitude.ToString(CultureInfo.InvariantCulture);
            offsetField.Value = offset.ToString();
            limitField.Value = limit.ToString();
            distanceField.Value = distance.ToString();

            return document.ToString();
        }

        internal static string UpdatePasswordRequest(User user, string password)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/UpdatePasswordTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("updateUserPassword", _sightseekerNS))
                .Element(XName.Get("token", ""));
            XElement passwordField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("updateUserPassword", _sightseekerNS))
                .Element(XName.Get("newPass"));
            tokenField.Value = user.Token;
            passwordField.Value = password;

            return document.ToString();
        }

        internal static string VisitTagRequest(User user, double longitude, double latitude, string date)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/VisitTagTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("visitTag", _sightseekerNS))
                .Element(XName.Get("token"));
            XElement dateField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("visitTag", _sightseekerNS))
                .Element(XName.Get("visitingDate"));
            XElement longitudeField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("visitTag", _sightseekerNS))
                .Element(XName.Get("longitude"));
            XElement latitudeField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("visitTag", _sightseekerNS))
                .Element(XName.Get("latitude"));
            tokenField.Value = user.Token;
            dateField.Value = date;
            longitudeField.Value = longitude.ToString(CultureInfo.InvariantCulture);
            latitudeField.Value = latitude.ToString(CultureInfo.InvariantCulture);

            return document.ToString();
        }

        internal static string GetVisitedTagsFromTokenRequest(User user, int offset, int limit)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/GetVisitedTagsFromTokenTemplate.xml");
            XElement requestField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("getVisitedTagsFromToken", _sightseekerNS));

            XElement tokenField = requestField.Element(XName.Get("token"));
            XElement offsetField = requestField.Element(XName.Get("offset"));
            XElement limitField = requestField.Element(XName.Get("limit"));

            tokenField.Value = user.Token;
            offsetField.Value = offset.ToString();
            limitField.Value = limit.ToString();

            return document.ToString();
        }

        internal static string CountVisitedTagsRequest(User user)
        {
            XDocument document = XDocument.Load(@"Assets/BackendRequestTemplates/CountVisitedTagsFromTokenTemplate.xml");
            XElement tokenField = document.Root.Element(XName.Get("Body", _soapEnvNS))
                .Element(XName.Get("countVisitedTagsFromToken", _sightseekerNS))
                .Element(XName.Get("token"));

            tokenField.Value = user.Token;

            return document.ToString();
        }
    }
}