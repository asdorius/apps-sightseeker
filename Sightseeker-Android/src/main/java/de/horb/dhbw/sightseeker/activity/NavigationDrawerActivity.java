package de.horb.dhbw.sightseeker.activity;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import de.horb.dhbw.sightseeker.database.Synchronizer;
import de.horb.dhbw.sightseeker.fragment.NavigationDrawerFragment;
import de.horb.dhbw.sightseeker.fragment.PlacesFragment;
import de.horb.dhbw.sightseeker.fragment.ProfileFragment;
import de.horb.dhbw.sightseeker.fragment.SettingsFragment;
import de.horb.dhbw.sightseeker.manager.WebserviceManager;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.support.Support;

public class NavigationDrawerActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;

    public boolean nfcAction = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        evaluateStart();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // Update the main content by replacing fragment
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = ProfileFragment.newInstance(position + 1);
                break;
            case 1:
                fragment = PlacesFragment.newInstance(position + 1);
                break;
            case 2:
                fragment = SettingsFragment.newInstance(position + 1);
                break;
        }
        startFragment(fragment);
    }

    public void onSectionAttached(int number) {
        String[] items = getResources().getStringArray(R.array.nav_drawer_items);
        switch (number) {
            case 1:
                mTitle = items[0];
                break;
            case 2:
                mTitle = items[1];
                break;
            case 3:
                mTitle = items[2];
                break;
            case 4:
                mTitle = items[3];
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout:
                // Try to logout from database
                logout();
                return true;
            case R.id.action_refresh:
                // Try to synchronize local storage with database
                synchronize();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        // TODO startet aktuell die App neu
        finish();
        System.exit(0);
    }

    // Puts a bundle of data to the new intent
    @Override
    public void startActivity(Intent intent) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ARG_INTENT_ACTION, getIntent().getExtras().
                getString(Constants.ARG_INTENT_ACTION));
        bundle.putBoolean(Constants.ARG_LOGGED_OUT, true);
        intent.putExtras(bundle);
        super.startActivity(intent);
    }

    // Create an "WebserviceManager" with method value "logout"
    // Try to logout from database
    private void logout() {
        WebserviceManager webserviceManager = new WebserviceManager(this,
                Constants.WEBSERVICE_TASK_LOGOUT);
        webserviceManager.doAccess();
    }

    // Handles the database access of "logout"
    public void handleAccessLogout() {
        // Don't check if access was successfull, simply delete token and start activity "WelcomeActivity"
        WebserviceManager.token = null;
        Support.userId = null;

        startActivity(new Intent(this, WelcomeActivity.class));
    }

    private void synchronize() {
        if (WebserviceManager.token == null) {
            Toast.makeText(this, Constants.ERROR_OFFLINE_MODE, Toast.LENGTH_SHORT).show();
        } else if (!Support.isNetworkAvailable(this)) {
            Toast.makeText(this, Constants.ERROR_NETWORK_FAILED, Toast.LENGTH_SHORT).show();
        } else {
            // Synchronize local storage with database
            Synchronizer synchronizer = new Synchronizer(this);
            synchronizer.synchronize();
        }
    }

    // Attaches fragment to parent activity "NavigationDrawerActivity"
    public void startFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    }

    private void evaluateStart() {
        boolean showPlaces = getIntent().getExtras().getBoolean(Constants.ARG_OFFLINE_MODE);
        // Start fragment "PlacesFragment" and handle the nfc message if nfc was discovered
        if (isNFCAction()) {
            nfcAction = true;
            if (getIntent().getData() != null) {
                startFragment(PlacesFragment.newInstance(2, getIntent()));
            } else {
                Toast.makeText(this, Constants.ANNOUNCE_LOST_NFC_DATA, Toast.LENGTH_SHORT).show();
            }
        } else if (showPlaces) {
            // Start fragment "PlacesFragment" if app was started in offline mode
            startFragment(PlacesFragment.newInstance(3));
        }
    }

    // Checks if a nfc message was discovered and the intent action is "ndef discovered"
    private boolean isNFCAction() {
        boolean ret = false;
        if (getIntent().getExtras().getString(Constants.ARG_INTENT_ACTION).
                equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
            ret = true;
        }
        return ret;
    }
}
