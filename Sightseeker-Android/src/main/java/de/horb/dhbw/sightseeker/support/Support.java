package de.horb.dhbw.sightseeker.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Simon on 14.04.2015.
 */
public class Support {

    // Saves the userId for the user logged in
    public static Long userId;

    private static final String DATABASE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    private static final String SHOW_OFF_FORMAT = "HH:mm:ss yyyy-MM-dd";

    // Checks if network is available
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }

    // Returns the system date time
    public static String getDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat(DATABASE_FORMAT);
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    // Formats the date time from local "database format" into "show off format"
    public static String formatDateTime(String _dateTime) {
        Date dateTime = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATABASE_FORMAT);
        try {
            dateTime = dateFormat.parse(_dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (dateTime != null) {
            dateFormat.applyPattern(SHOW_OFF_FORMAT);
            String dateTimeString = dateFormat.format(dateTime);
            return dateTimeString;
        }
        return null;
    }
}
