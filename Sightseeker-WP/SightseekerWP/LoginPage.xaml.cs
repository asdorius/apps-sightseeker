﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ServiceModel;

namespace Sightseeker
{
    public partial class Page1 : PhoneApplicationPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        private string _email;
        private string _password;

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await App.User.Login(EmailTextBox.Text, PasswordTextBox.Text);
            NavigationService.GoBack();
        }
    }
}