package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class App_User_Result extends SoapObject {
    private java.lang.Integer returnCode;

    private de.horb.dhbw.sightseeker.webservice.App_User user;

    public App_User_Result() {
        super("", "");
    }
    public void setReturnCode(java.lang.Integer returnCode) {
        this.returnCode = returnCode;
    }

    public java.lang.Integer getReturnCode() {
        return this.returnCode;
    }

    public void setUser(de.horb.dhbw.sightseeker.webservice.App_User user) {
        this.user = user;
    }

    public de.horb.dhbw.sightseeker.webservice.App_User getUser() {
        return this.user;
    }

    public int getPropertyCount() {
        return 2;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return returnCode;
        case 1: return user;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: returnCode = (java.lang.Integer) __obj; break;
        case 1: user = (de.horb.dhbw.sightseeker.webservice.App_User) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "returnCode";
            __info.type = java.lang.Integer.class; break;
        case 1:
            __info.name = "user";
            __info.type = de.horb.dhbw.sightseeker.webservice.App_User.class; break;
        }
    }

}
