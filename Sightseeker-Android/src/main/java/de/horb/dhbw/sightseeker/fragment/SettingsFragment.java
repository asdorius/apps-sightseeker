package de.horb.dhbw.sightseeker.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import de.horb.dhbw.sightseeker.activity.NavigationDrawerActivity;
import de.horb.dhbw.sightseeker.data.User;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.manager.WebserviceManager;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.webservice.WebserviceTaskResult;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private View    mProgressView;
    private View    mFormView;
    private EditText mPasswordOldView;
    private EditText mPasswordNewView;
    private EditText mPasswordNew2View;
    private Button  mButtonChangePassword;

    private String  mPasswordOld;
    private String  mPasswordNew;
    private String  mPasswordNew2;

    private DatabaseHandler mDbHandler;
    private User mUser;


    public static SettingsFragment newInstance(int sectionNumber) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDbHandler = new DatabaseHandler(getActivity());
        mDbHandler.openDataBase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        mUser = mDbHandler.getUser();

        mFormView = rootView.findViewById(R.id.layout_settings);
        mProgressView = rootView.findViewById(R.id.progressBar_settings);
        mPasswordOldView = (EditText) rootView.findViewById(R.id.password_old);
        mPasswordNewView = (EditText) rootView.findViewById(R.id.password_new);
        mPasswordNew2View = (EditText) rootView.findViewById(R.id.password_new2);
        mButtonChangePassword = (Button) rootView.findViewById(R.id.settings_button_changePassword);

        mButtonChangePassword.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                attemptUpdatePassword();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationDrawerActivity) activity).onSectionAttached(getArguments().getInt(Constants.ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragment contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragment/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void attemptUpdatePassword() {
        // Reset errors.
        mPasswordOldView.setError(null);
        mPasswordNewView.setError(null);
        mPasswordNew2View.setError(null);

        mPasswordOld    = null;
        mPasswordNew    = null;
        mPasswordNew2   = null;

        // Store values at the time of the login attempt.
        mPasswordOld = mPasswordOldView.getText().toString();
        mPasswordNew = mPasswordNewView.getText().toString();
        mPasswordNew2 = mPasswordNew2View.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid username, if the user entered one.
        if (TextUtils.isEmpty(mPasswordOld)) {
            mPasswordOldView.setError(getString(R.string.error_field_required));
            focusView = mPasswordOldView;
            cancel = true;
        } else if (!mUser.getPassword().equals(mPasswordOld)) {
            // Check if user entered the old password rightly
            mPasswordOldView.setError(getString(R.string.error_invalid_old_password));
            focusView = mPasswordOldView;
            cancel = true;
        } else if (TextUtils.isEmpty(mPasswordNew) || !isPasswordValid(mPasswordNew)) {
            // Check for a valid email address.
            mPasswordNewView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordNewView;
            cancel = true;
        } else if (TextUtils.isEmpty(mPasswordNew2) || !isPasswordValid(mPasswordNew2)) {
            // Check for a valid password, if the user entered one.
            mPasswordNew2View.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordNew2View;
            cancel = true;
        } else if (!mPasswordNew.equals(mPasswordNew2)) {
            // Check if new password is confirmed
            mPasswordNew2View.setError(getString(R.string.error_invalid_new_password));
            focusView = mPasswordNew2View;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt updatePassword and focus the first form field with an error.
            focusView.requestFocus();
        } else {
            updatePassword();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    // Create an "WebserviceManager" with method value "updatePassword"
    // Try to update the password
    private void updatePassword() {
        WebserviceManager webserviceManager = new WebserviceManager(getActivity(),
                Constants.WEBSERVICE_TASK_UPDATE_PASSWORD, mPasswordNew);
        webserviceManager.doAccess();
    }

    // Handles the database access of "updatePassword" by a "WebserviceTaskResult"
    public void handleUpdatePassword(WebserviceTaskResult taskResult) {
        if (taskResult.getReturnCodeResult() != null) {
            int returnCode = taskResult.getReturnCodeResult().getReturnCode();

            // Update the password in local database if the access was successfull
            if (WebserviceManager.accessSuccessfull(returnCode)) {
                Toast.makeText(getActivity(), Constants.ANNOUNCE_PASSWORD_CHANGED, Toast.LENGTH_SHORT).show();
                mDbHandler.updateUserPassword(mPasswordNew);
            } else {
                WebserviceManager.toastErrorByReturnCode(getActivity(), returnCode);
            }
        } else {
            WebserviceManager.toastErrorByReturnCode(getActivity(), Constants.RETURN_CODE_UNKNOWN_ERROR);
        }
    }

    public void refresh() {
        // TODO refresh view

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
