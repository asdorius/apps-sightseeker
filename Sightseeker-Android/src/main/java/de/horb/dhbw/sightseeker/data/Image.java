package de.horb.dhbw.sightseeker.data;

/**
 * Created by Simon on 20.05.2015.
 * Represents the datatype for a record in the table "image"
 */
public class Image {

    private Long    mId;
    private Integer mTagId;
    private byte[]  mImageByteArray;

    public byte[] getImageByteArray() {
        return mImageByteArray;
    }

    public void setImageByteArray(byte[] imageByteArray) {
        this.mImageByteArray = imageByteArray;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        this.mId = id;
    }

    public Integer getTagId() {
        return mTagId;
    }

    public void setTagId(Integer tagId) {
        this.mTagId = tagId;
    }
}
