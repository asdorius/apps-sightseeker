package de.horb.dhbw.sightseeker.database;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import java.util.List;

import de.horb.dhbw.sightseeker.activity.NavigationDrawerActivity;
import de.horb.dhbw.sightseeker.data.Place;
import de.horb.dhbw.sightseeker.data.User;
import de.horb.dhbw.sightseeker.fragment.PlacesFragment;
import de.horb.dhbw.sightseeker.fragment.ProfileFragment;
import de.horb.dhbw.sightseeker.fragment.SettingsFragment;
import de.horb.dhbw.sightseeker.manager.WebserviceManager;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.support.Support;
import de.horb.dhbw.sightseeker.webservice.App_Tag;
import de.horb.dhbw.sightseeker.webservice.App_User;
import de.horb.dhbw.sightseeker.webservice.WebserviceTaskResult;

/**
 * Created by Simon on 09.05.2015.
 * The class is used for synchronizing user and place data between the database and the local storage.
 */
public class Synchronizer {

    private SynchronizeTask mSynchronizeTask;
    private DatabaseHandler mDbHandler;

    private Context     mContext;
    private App_User    mAppUser;
    private App_Tag[]   mAppTags;

    private List<Integer> mAllLocalTagIds;
    private List<Place> mUnsyncedPlaces;
    private List<Integer> mNoImageTagIds;

    private int         mReturnCodeUser;
    private int         mReturnCodeTags;

    // Contructor which requires a context for different callbacks
    public Synchronizer(Context context) {
        mContext = context;
    }

    // Triggers the synchronization by fetching the local storage and the data from database
    public void synchronize() {
        if (WebserviceManager.token == null) {
            Toast.makeText(mContext, Constants.ERROR_OFFLINE_MODE, Toast.LENGTH_SHORT).show();
            return;
        }
        showProgress(true);
        getLocalDatabase();
        getDatabase();
    }

    // Open the local database
    private void getLocalDatabase() {
        mDbHandler = new DatabaseHandler(mContext);
        mDbHandler.openDataBase();

        // All tag ids from local database (represents all synchronized places)
        mAllLocalTagIds = mDbHandler.getAllTagIds();
        // All unsynced places (attribute tag_id in the table place is still nil)
        mUnsyncedPlaces = mDbHandler.getUnsyncedPlaces();
        // All places which don't refer to an image (attribute image in the table place is still nil)
        mNoImageTagIds = mDbHandler.getNoImagePlaces();
    }

    // Create an "WebserviceManager" with method value "getDatabase"
    // Try to fetch the user data and all places saved in the database
    private void getDatabase() {
        WebserviceManager manager = new WebserviceManager(this, mContext,
                Constants.WEBSERVICE_TASK_GET_DATABASE, -1, -1);
        manager.doAccess();
    }

    // Create an "WebserviceManager" with method value "visitTag"
    // Try to insert a record into database by the data retrieved from the nfc scan (lat, lon, datTime)
    private void visitTag(Place place) {
        WebserviceManager manager = new WebserviceManager(this, mContext,
                Constants.WEBSERVICE_TASK_VISIT_TAG,
                place.getLatitude(), place.getLongitude(), place.getDateTime());
        manager.doAccess();
    }

    // Create an "WebserviceManager" with method value "getPictureLink"
    // Try to fetch the picture for the passed parameter tagId which represents a place in the database
    private void getPictureLink(Integer tagId) {
        WebserviceManager manager = new WebserviceManager(this, mContext,
                Constants.WEBSERVICE_TASK_GET_PICTURE_LINK, tagId);
        manager.doAccess();
    }

    // Handles the database access of "getDatabase" by a "WebserviceTaskResult"
    public void handleGetDatabase(WebserviceTaskResult taskResult) {
        if (taskResult.getAppUserResult() != null) {
            mReturnCodeUser = taskResult.getAppUserResult().getReturnCode();
            mAppUser = taskResult.getAppUserResult().getUser();
        }

        if (taskResult.getAppUserTagsResult() != null) {
            mReturnCodeTags = taskResult.getAppUserTagsResult().getReturnCode();
            mAppTags = taskResult.getAppUserTagsResult().getVisitedTags();
        }


        if (mAppUser != null || mAppTags != null || mUnsyncedPlaces.size() != 0 ||
                (mAppTags == null && mReturnCodeTags == Constants.RETURN_CODE_NO_PLACES_FOUND)) {
            mSynchronizeTask = new SynchronizeTask();
            mSynchronizeTask.execute((Void) null);
        } else {
            showProgress(false);
            Toast.makeText(mContext, Constants.ERROR_SYNCHRONIZE_FAILED, Toast.LENGTH_SHORT).show();
        }
    }

    // Handles the database access of "visitTag" by a "WebserviceTaskResult"
    public void handleVisitTag(WebserviceTaskResult taskResult) {
        if (taskResult.getAppTagResult() != null) {
            int returnCode = taskResult.getAppTagResult().getReturnCode();

            // If the access was successfull update the table "place" and the table "user" by the retrieved place data
            // and try to get the related picture by tagId if it doesn't exist in the local database
            if (WebserviceManager.accessSuccessfull(returnCode)) {
                App_Tag appTag = taskResult.getAppTagResult().getTag();
                if (appTag != null) {
                    if (!mDbHandler.existsPicture(appTag.getID())) {
                        getPictureLink(appTag.getID());
                    }
                    mDbHandler.updatePlaceByAppTag(appTag);
                    mDbHandler.updateUserByPlace(Place.createPlaceByAppTag(appTag));
                }
            }
        }
    }

    // Handles the database access of "getPictureLink" by a "WebserviceTaskResult"
    public void handleGetPictureLink(WebserviceTaskResult taskResult) {
        // Insert the picture into local database if the result "byteArrayBuffer" is not null
        if (taskResult.getImageByteArrayBuffer() != null) {
            ContentValues values = new ContentValues();
            values.put(DatabaseHandler.IMAGE_COLUMN_TAG_ID, taskResult.getTagId());
            values.put(DatabaseHandler.IMAGE_COLUMN_IMAGE, taskResult.getImageByteArrayBuffer().toByteArray());
            mDbHandler.createImage(values);
        } else {
            Toast.makeText(mContext, Constants.ERROR_LOADING_IMAGE, Toast.LENGTH_SHORT).show();
        }
    }

    // Calls the method "showProgress" in different classes by switching the initial context
    private void showProgress(Boolean show) {
        NavigationDrawerActivity activity = (NavigationDrawerActivity) mContext;
        FragmentManager manager = activity.getSupportFragmentManager();

        switch (manager.findFragmentById(R.id.container).getClass().getSimpleName()) {
            case Constants.FRAGMENT_PROFILE:
                ProfileFragment profileFragment = (ProfileFragment) manager.findFragmentById(R.id.container);
                if (profileFragment != null) {
                    profileFragment.refresh();
                    profileFragment.showProgress(show);
                }
                break;
            case Constants.FRAGMENT_PLACES:
                PlacesFragment placesFragment = (PlacesFragment) manager.findFragmentById(R.id.container);
                if (placesFragment != null) {
                    placesFragment.showProgress(show);
                }
                break;
            case Constants.FRAGMENT_SETTINGS:
                SettingsFragment settingsFragment = (SettingsFragment) manager.findFragmentById(R.id.container);
                if (settingsFragment != null) {
                    settingsFragment.showProgress(show);
                }
                break;
            default:
                return;
        }
    }

    // Asynchronous task which synchronizes the local storage with the database
    public class SynchronizeTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            // Synchronize User
            User user = mDbHandler.getUser();

            // Udate the user name if the access for "getUser" in "getDatabase" was successfull
            // and the user is still not synced
            if (WebserviceManager.accessSuccessfull(mReturnCodeUser) && mAppUser != null) {
                if (user.getSynced() == DatabaseHandler.VALUE_NOT_SYNCED) {
                    mDbHandler.updateUser(mAppUser);
                }
            }

            // Synchronize Places
            if (WebserviceManager.accessSuccessfull(mReturnCodeTags) && mAppTags != null) {

                // Fetch unsynced places from database (i.e. from another device)
                for (int i = 0; i < mAppTags.length; i++) {
                    ContentValues values = new ContentValues();

                    // Create place in local storage if the list of all tag id's doesn't contain the
                    // tag id retrieved from the database
                    if (!mAllLocalTagIds.contains(mAppTags[i].getID())) {
                        values = setContentValues(mAppTags[i]);
                        Place newPlace = mDbHandler.createPlace(values);
                        mDbHandler.updateUserByPlace(newPlace);
                    }
                }
            }

            // Try to insert unsynced places from local storage into database (i.e. no network at scan time)
            for (Place place : mUnsyncedPlaces) {
                visitTag(place);
            }

            // Try to get the picture of all places having no related picture in local database
            for (Integer tagId : mNoImageTagIds) {
                getPictureLink(tagId);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mSynchronizeTask = null;
            // Hide progress bar
            showProgress(false);
            if (!WebserviceManager.accessSuccessfull(mReturnCodeUser) &&
                    !WebserviceManager.accessSuccessfull(mReturnCodeTags)) {
                Toast.makeText(mContext, Constants.ERROR_SYNCHRONIZE_FAILED, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mSynchronizeTask = null;
            // Hide progress bar
            showProgress(false);
            Toast.makeText(mContext, Constants.ERROR_SYNCHRONIZE_FAILED, Toast.LENGTH_SHORT).show();
        }

        // Set the content values by the retrieved place (class "App_Tag")
        private ContentValues setContentValues(App_Tag appTag) {
            ContentValues values = new ContentValues();
            values.put(DatabaseHandler.PLACE_COLUMN_TAG_ID, appTag.getID());
            values.put(DatabaseHandler.PLACE_COLUMN_PLACE, appTag.getName());
            values.put(DatabaseHandler.PLACE_COLUMN_POINTS, appTag.getScore());
            values.put(DatabaseHandler.PLACE_COLUMN_LATITUDE, appTag.getLatitude());
            values.put(DatabaseHandler.PLACE_COLUMN_LONGITUDE, appTag.getLongitude());
            values.put(DatabaseHandler.PLACE_COLUMN_DATETIME, appTag.getVisitingDate());
            values.put(DatabaseHandler.PLACE_COLUMN_USER_ID, Support.userId);
            values.put(DatabaseHandler.PLACE_COLUMN_SYNCED, DatabaseHandler.VALUE_SYNCED);
            return values;
        }
    }
}
