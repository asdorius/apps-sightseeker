<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Cover Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h1>SightSeeker</h3>
              <nav>
                <ul class="nav masthead-nav">
                </ul>
              </nav>
            </div>
          </div>

        <div class="inner cover">
            <form class="form-horizontal">
        <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<h3>Passwort vergessen</h3>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="emailinput">Email</label>  
  <div class="col-md-4">
  <input id="emailinput" name="emailinput" placeholder="Max@Mustermann.de" class="form-control input-md" required="" type="text">
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="resetbutton"></label>
  <div class="col-md-4">
    <button id="resetbutton" name="resetbutton" class="btn btn-success">Anfordern</button>
    <button id="backbutton" name="backbutton" class="btn btn-danger">Zurück</button>
  </div>
</div>

</fieldset>
</form>

            

          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>&copy; SightSeeker-App 2015 DHBW-Horb.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
