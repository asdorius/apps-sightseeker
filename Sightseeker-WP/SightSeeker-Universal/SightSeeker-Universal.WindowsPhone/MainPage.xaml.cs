﻿#region usings

using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using SightSeeker_Universal.Common;
using SightSeeker_Universal.Specific_Logic;

#endregion

// Die Projektvorlage "Universelle Hub-Anwendung" ist unter http://go.microsoft.com/fwlink/?LinkID=391955 dokumentiert.

namespace SightSeeker_Universal
{
    /// <summary>
    ///     Eine Seite, auf der eine gruppierte Auflistung von Elementen angezeigt wird.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly App app = Application.Current as App;
        private readonly ObservableDictionary defaultViewModel = new ObservableDictionary();
        private readonly CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
        private readonly NavigationHelper navigationHelper;
        private readonly ObservableCollection<AppTag> nearTags = new ObservableCollection<AppTag>();
        private readonly ResourceLoader resourceLoader = ResourceLoader.GetForCurrentView("Resources");
        private readonly ObservableCollection<AppTag> visitedTags = new ObservableCollection<AppTag>();

        public MainPage()
        {
            InitializeComponent();

            NavigationCacheMode = NavigationCacheMode.Required;

            navigationHelper = new NavigationHelper(this);
            navigationHelper.LoadState += NavigationHelper_LoadState;
            navigationHelper.SaveState += NavigationHelper_SaveState;

            nearTagsList.ItemsSource = nearTags;
            visitedTagsList.ItemsSource = visitedTags;

        }

        /// <summary>
        ///     Ruft den <see cref="NavigationHelper" /> ab, der mit dieser <see cref="Page" /> verknüpft ist.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return navigationHelper; }
        }

        /// <summary>
        ///     Ruft das Anzeigemodell für diese <see cref="Page" /> ab.
        ///     Dies kann in ein stark typisiertes Anzeigemodell geändert werden.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return defaultViewModel; }
        }

        /// <summary>
        ///     Füllt die Seite mit Inhalt auf, der bei der Navigation übergeben wird.  Gespeicherte Zustände werden ebenfalls
        ///     bereitgestellt, wenn eine Seite aus einer vorherigen Sitzung neu erstellt wird.
        /// </summary>
        /// <param name="sender">
        ///     Die Quelle des Ereignisses, normalerweise <see cref="NavigationHelper" />
        /// </param>
        /// <param name="e">
        ///     Ereignisdaten, die die Navigationsparameter bereitstellen, die an
        ///     <see cref="Frame.Navigate(Type, object)" /> als diese Seite ursprünglich angefordert wurde und
        ///     ein Wörterbuch des Zustands, der von dieser Seite während einer früheren
        ///     beibehalten wurde.  Der Zustand ist beim ersten Aufrufen einer Seite NULL.
        /// </param>
        private async void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        ///     Behält den dieser Seite zugeordneten Zustand bei, wenn die Anwendung angehalten oder
        ///     die Seite im Navigationscache verworfen wird.  Die Werte müssen den Serialisierungsanforderungen
        ///     von <see cref="SuspensionManager.SessionState" /> entsprechen.
        /// </summary>
        /// <param name="sender">Die Quelle des Ereignisses, normalerweise <see cref="NavigationHelper" /></param>
        /// <param name="e">
        ///     Ereignisdaten, die ein leeres Wörterbuch zum Auffüllen bereitstellen
        ///     serialisierbarer Zustand.
        /// </param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        /// <summary>
        /// Behandelt den Klick auf den Change Password Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void changePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            ChangePasswordDialog changepw = new ChangePasswordDialog();
            ContentDialogResult changepwres = await changepw.ShowAsync();
        }

        /// <summary>
        /// Behandelt den Klick auf den Button "Logout"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void logoutButton_Click(object sender, RoutedEventArgs e)
        {

            await Backend.LogoutUser(app.LoggedInUser);
            app.LoggedInUser = null;
            nearTags.Clear();
            visitedTags.Clear();
            FillProfile();
            await EnsureUserLoggedIn();
            LoadMoreNearTags();
            LoadMoreVisitedTags();
            FillProfile();
        }

        #region NavigationHelper-Registrierung

        /// <summary>
        ///     Die in diesem Abschnitt bereitgestellten Methoden werden einfach verwendet, um
        ///     damit NavigationHelper auf die Navigationsmethoden der Seite reagieren kann.
        ///     <para>
        ///         Seitenspezifische Logik in Ereignishandlern für
        ///         <see cref="NavigationHelper.LoadState" />
        ///         und <see cref="NavigationHelper.SaveState" />.
        ///         Der Navigationsparameter ist in der LoadState-Methode verfügbar
        ///         zusätzlich zum Seitenzustand, der während einer früheren Sitzung beibehalten wurde.
        ///     </para>
        /// </summary>
        /// <param name="e">Ereignisdaten, die beschreiben, wie diese Seite erreicht wurde.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);

            if (app.LoggedInUser == null)
            {
                await EnsureUserLoggedIn();
            }
            NFCReader reader = new NFCReader();
            reader.TagVisited += TagVisited;
            LoadMoreVisitedTags();
            LoadMoreNearTags();
            FillProfile();
        }

        /// <summary>
        /// Wird ausgelöst, wenn der NFCReader meldet, dass ein Tag besucht wurde.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TagVisited(object sender, EventArgs e)
        {
            dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                LoadMoreVisitedTags();
                FillProfile();
            });
        }

        /// <summary>
        /// Füllt das Profil mit den Benutzerdaten, wenn ein Benutzer verfübar ist. Löscht diese sonst.
        /// </summary>
        private async void FillProfile()
        {
            if (app.LoggedInUser != null)
            {
                userNameTextBlock.Text = app.LoggedInUser.UserName;
                scoreTextBlock.Text = (await Backend.GetUserScoreFromToken(app.LoggedInUser)).ToString();
            }
            else
            {
                userNameTextBlock.Text = "";
                scoreTextBlock.Text = "";
            }
        }

        /// <summary>
        /// Lädt bis zu 20 weitere besuchte Tags in das Datenmodell.
        /// </summary>
        private async void LoadMoreVisitedTags()
        {
            foreach (AppTag tag in await Backend.GetVisitedTagsFromUser(app.LoggedInUser, visitedTags.Count, 20))
            {
                try
                {
                    tag.ImageLink = await Backend.GetPictureLinkFromTag(app.LoggedInUser, tag);
                    tag.Description = await Backend.GetInfoTextFromTag(app.LoggedInUser, tag);
                    visitedTags.Add(tag);
                }
                catch { }
            }
        }

        /// <summary>
        /// Lädt bis zu 20 weitere Tags in einem Umkreis von 5000km in das Datenmodell
        /// </summary>
        private async void LoadMoreNearTags()
        {
            foreach (AppTag tag in await Backend.GetNearTags(app.LoggedInUser, nearTags.Count, 20, 5000))
            {
                try
                {
                    tag.ImageLink = await Backend.GetPictureLinkFromTag(app.LoggedInUser, tag);
                    tag.Description = await Backend.GetInfoTextFromTag(app.LoggedInUser, tag);
                    nearTags.Add(tag);
                }
                catch { }
            }
        }

        /// <summary>
        /// Das erwarten dieser Methode sichert, dass ein Benutzer eingeloggt ist und auf app.LoggedInUser sicher zugegriffen werden kann.
        /// </summary>
        /// <returns>Task, macht diese Methode Awaitable</returns>
        private async Task EnsureUserLoggedIn()
        {
            bool loginNeeded = false;

            try
            {
                app.LoggedInUser = await Backend.LoginAndGetSavedUser();
            }
            catch
            {
                loginNeeded = true;
            }
            LoginDialog login = new LoginDialog();
            ContentDialogResult loginres = ContentDialogResult.None;

            while (app.LoggedInUser == null)
            {
                if (loginNeeded)
                {
                    loginres = await login.ShowAsync();
                }

                if (loginres == ContentDialogResult.Primary)
                {
                    try
                    {
                        app.LoggedInUser = await Backend.LoginAndGetUser(login.EmailData, login.PasswordData);
                        loginNeeded = false;
                    }
                    catch (LoginFailedException exc)
                    {
                        new MessageDialog("Login failed (wrong Email/Password?)", "Warning!").ShowAsync();
                        loginNeeded = true;
                    }
                    catch (DoubleOptInException exc)
                    {
                        new MessageDialog("User not confirmed", "Warning!").ShowAsync();
                        loginNeeded = true;
                    }
                    catch (Exception exc)
                    {
                        loginNeeded = true;
                    }
                }
                else
                {
                    RegisterDialog register = new RegisterDialog();
                    ContentDialogResult registerres = await register.ShowAsync();
                    if (registerres == ContentDialogResult.Secondary)
                    {
                        loginres = await login.ShowAsync();
                    }
                }
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        /// <summary>
        /// Schließt das Detail Popup eines Tags. Wird bei Klick auf dessen Close Button ausgeführt.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            detailPopup.IsOpen = false;
        }

        /// <summary>
        /// Wird ausgelöst, wenn auf ein Element in einer AppTag-Liste geklickt wird. Zeigt den DetailPopup mit den Details zu diesem Tag an.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {

            Grid send = (sender as Grid);
            detailPopup.DataContext = send.DataContext;
            popupGrid.Width = mainGrid.ActualWidth;
            popupGrid.Height = mainGrid.ActualHeight;
            detailPopup.IsOpen = true;
        }

        /// <summary>
        /// Wird beim Klick auf den Load More Butten unter der NearTagsListe ausgelöst.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadMoreNearTagsText_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            LoadMoreNearTags();
        }

        /// <summary>
        /// Wird beim Klick auf den Load More Butten unter der VisitedTagsListe ausgelöst.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadMoreVisitedTagsText_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            LoadMoreVisitedTags();
            FillProfile();
        }

    }
}