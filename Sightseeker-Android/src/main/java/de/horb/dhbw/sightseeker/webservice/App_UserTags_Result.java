package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class App_UserTags_Result extends SoapObject {
    private java.lang.Integer returnCode;

    private de.horb.dhbw.sightseeker.webservice.App_Tag[] visitedTags;

    public App_UserTags_Result() {
        super("", "");
    }
    public void setReturnCode(java.lang.Integer returnCode) {
        this.returnCode = returnCode;
    }

    public java.lang.Integer getReturnCode() {
        return this.returnCode;
    }

    public void setVisitedTags(de.horb.dhbw.sightseeker.webservice.App_Tag[] visitedTags) {
        this.visitedTags = visitedTags;
    }

    public de.horb.dhbw.sightseeker.webservice.App_Tag[] getVisitedTags() {
        return this.visitedTags;
    }

    public int getPropertyCount() {
        return 2;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return returnCode;
        case 1: return visitedTags;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: returnCode = (java.lang.Integer) __obj; break;
        case 1: visitedTags = (de.horb.dhbw.sightseeker.webservice.App_Tag[]) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "returnCode";
            __info.type = java.lang.Integer.class; break;
        case 1:
            __info.name = "visitedTags";
            __info.type = de.horb.dhbw.sightseeker.webservice.App_Tag[].class; break;
        }
    }

}
