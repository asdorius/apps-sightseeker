<?php
	require_once 'Zend/Soap/Client.php';
	session_start();

  	if(!isset($_SESSION['logedIn']) || !isset($_SESSION['loginToken']))
	{
		header("Redirect to login Page");
   		header("Location: login.php");
    	exit;
	}
	else
	{
		$client = new Zend_Soap_Client("http://localhost/sightseeker/server.php?wsdl");
	}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="erde_logo.png">

    <title>Sightseeker - Index</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">SightSeeker</h3>
              <nav>
                <ul class="nav masthead-nav">
                  <li class="active"><a href="index.php">Home</a></li>
                  <li><a href="user.php">User</a></li>
                  <li><a href="places.php">Meine Orte</a></li>
                  <li><a href="map.php">Karte</a></li>
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
          <?php

            		// Frage Usernamen ab
            		$username = "";
					$userC = $client->getUserFromToken($_SESSION['loginToken']);
    				$rCU = $userC -> returnCode;
    				if ($rCU == 0)
    				{
    					$user = $rCU->user;
    					$username = $user -> Username;
    				}

            		echo "<h1 class=\"cover-heading\">Willkommen bei Sightseeker $username</h1>";
            		echo "<p class=\"lead\">Hier können Sie ihre bereits besuchten Orte auf einer Karte anzeigen lassen.</p>";
            		echo "<p class=\"lead\">";
            		echo "<a href=\"map.php\" class=\"btn btn-lg btn-default\">Zur Karte</a>";
            		echo "</p>";

            ?>
          </div>
          <div class="mastfoot">
            <div class="inner">
              <p>&copy; SightSeeker-App 2015 DHBW-Horb.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>