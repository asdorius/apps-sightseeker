<?php
    session_start();

    $_SESSION['registerFlag'] = 1;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Sightseeker - Registrieren</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h1>SightSeeker</h3>
              <nav>
                <ul class="nav masthead-nav">
                </ul>
              </nav>
            </div>
          </div>

        <div class="inner cover">
        <form method="post" action="user_registered.php" class="form-horizontal">
        <fieldset>

        <!-- Form Name -->
        <h3>Registrieren</h3>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>  
            <div class="col-md-4">
             <input id="email" name="email" placeholder="Max@Mustermann.de" class="form-control input-md" required="" type="email" x-moz-errormessage="Bitte geben Sie eine gültige Emailadresse ein.">
    
            </div>
        </div>

        <!-- Text input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="username">Username</label>  
            <div class="col-md-4">
             <input id="username" name="username" placeholder="Maxi Musti" class="form-control input-md" required="" type="text" x-moz-errormessage="Bitte geben Sie eine gültign Usernamen ein">
    
            </div>
        </div>

        <!-- Password input-->
        <div class="form-group">
            <label class="col-md-4 control-label" for="pass">Passwort</label>
            <div class="col-md-4">
              <input id="passwordinput" name="pass" placeholder="123456789" class="form-control input-md" required="" type="password" x-moz-errormessage="Bitte geben Sie ein gültigs Passwort ein.">
    
            </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="registerButton"></label>
            <div class="col-md-1">
              <button id="registerButton" name="registerButton" class="btn btn-warning">Registrieren</button>
            </div>
        </div>
        
        </fieldset>
      </form>

          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>&copy; SightSeeker-App 2015 DHBW-Horb.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>