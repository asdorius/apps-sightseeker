﻿#region usings

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SightSeeker_Universal.Common;

#endregion

// Die Elementvorlage "Inhaltsdialog" ist unter "http://go.microsoft.com/fwlink/?LinkID=390556" dokumentiert.

namespace SightSeeker_Universal
{
    public sealed partial class ChangePasswordDialog : ContentDialog
    {
        public ChangePasswordDialog()
        {
            InitializeComponent();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            try
            {
                Backend.UpdatePassword((Application.Current as App).LoggedInUser, password.Password);
            }
            catch
            {
                args.Cancel = true;
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}