<?php
	require_once 'Zend/Soap/Client.php';
	session_start();

	$_SESSION['logedIn'] = 'false';

	if(isset($_SESSION['logedIn']) && isset($_SESSION['loginToken']))
	{
		header("Redirect to index Page");
   		header("Location: index.php");
    	exit;
	}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Sightseeker - Login</title>

    <!-- Bootstrap core CSS -->
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
      <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
      <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h1>SightSeeker</h3>
              <nav>
                <ul class="nav masthead-nav">
                </ul>
              </nav>
            </div>
          </div>

          <div class="inner cover">
          <?php
          		echo "<form method=\"post\" action=\"login.php\" class=\"form-signin\">";
          		echo "<h2 class=\"form-signin-heading\">Login</h2>";
          		echo "<label for=\"inputEmail\" class=\"sr-only\">Email</label>";
          		echo "<input name=\"email\" type=\"text\" id=\"inputEmail\" class=\"form-control\" placeholder=\"Max@Mustermann.de\" x-moz-errormessage=\"Bitte geben Sie eine gültige Emailadresse ein.\" required autofocus>";
          		echo "<label for=\"inputPassword\" class=\"sr-only\">Passwort</label>";
          		echo "<input name=\"pass\" type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Passwort\" x-moz-errormessage=\"Bitte geben Sie ein gültiges Pawwort ein.\" required>";
          		/*
          		echo "<div class=\"checkbox\">";
          		echo "<label>";
          		echo "<input type=\"checkbox\" value=\"remember-me\"> Angemeldet bleiben";
          		echo "</label>";
          		echo "</div>";
          		*/
          		echo "<br>";
          		echo "<button name=\"loginButton\" class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Anmelden</button>";
          		echo "<a href=\"forgotpassword.php\">Passwort vergessen</a> / <a href=\"register.php\">Registrieren</a>";
          		echo "</form>";

          ?>


      			

          </div>

          <div class="mastfoot">
            <div class="inner">
              <p>&copy; SightSeeker-App 2015 DHBW-Horb.</p>
            </div>
          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
      <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
<?php
 	function login()
	{
		$client = new Zend_Soap_Client("http://localhost/sightseeker/server.php?wsdl");
    	//$_SESSION['client'] = $client; 
  		//print_r($client);

    	$c = $client->login($_POST["email"], $_POST["pass"]);
    	$rC = $c -> returnCode;
    	// Wenn User eingeloggt
    	if ($rC == 0) 
    	{
    		$_SESSION['logedIn'] = 'true';
    		$_SESSION['loginToken'] = $c -> result;

    		header("Redirect to index Page");
    		header("Location: index.php");
    		exit;
    	}
    	// Wenn Fehler aufgetreten oder Benutzerdaten falsch
   		else
    	{
        	$_SESSION['logedIn'] = 'false';

        	header("Redirect to login_failed Page");
    		header("Location: login_failed.php");
    		exit;
    	}
	}
	if(isset($_POST['loginButton']))
	{
   		login();
	} 

?>