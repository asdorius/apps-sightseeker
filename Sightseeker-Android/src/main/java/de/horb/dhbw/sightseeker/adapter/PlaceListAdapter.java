package de.horb.dhbw.sightseeker.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simon.sightseeker.R;

import java.util.List;

import de.horb.dhbw.sightseeker.data.Image;
import de.horb.dhbw.sightseeker.data.Place;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.support.Support;

/**
 * Created by Simon on 09.05.2015.
 * This class represents the ListAdapter for the fragment "PlacesFragment"
 * It shows all places saved in the local database
 */
public class PlaceListAdapter extends ArrayAdapter<Place> {

    private final Context context;
    private final List<Place> places;
    private DatabaseHandler mDbHandler;

    public PlaceListAdapter(Context context, int resource, List<Place> places) {
        super(context, resource, places);
        this.context = context;
        this.places = places;
        // Open the local database
        mDbHandler = new DatabaseHandler(context);
        mDbHandler.openDataBase();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        if (rootView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rootView = inflater.inflate(R.layout.place_item, null);
        }

        Place place = places.get(position);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.place_imageView);
        TextView placeView = (TextView) rootView.findViewById(R.id.place_textView);
        TextView latitude_textView = (TextView) rootView.findViewById(R.id.latitude_textView);
        TextView longitude_textView = (TextView) rootView.findViewById(R.id.longitude_textView);
        TextView dateTimeView = (TextView) rootView.findViewById(R.id.date_time_textView);
        TextView pointsView = (TextView) rootView.findViewById(R.id.points_textView);

        // Try to get a related image from local database
        Image image = mDbHandler.getImageByTagId(place.getTagId());

        if (imageView != null && image != null) {
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(image.getImageByteArray(),
                    0, image.getImageByteArray().length));
        } else {
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.image_places));
        }

        if (placeView != null) {
            placeView.setText(place.getPlace());
        }
        if (latitude_textView != null) {
            latitude_textView.setText("Breitengrad: " + place.getLatitude());
        }
        if (longitude_textView != null) {
            longitude_textView.setText("Längengrad: " + place.getLongitude());
        }
        if (dateTimeView != null) {
            dateTimeView.setText(Support.formatDateTime(place.getDateTime()));
        }
        if (pointsView != null) {
            pointsView.setText(Integer.toString(place.getPoints()));
        }

        return rootView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
