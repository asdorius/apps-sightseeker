package de.horb.dhbw.sightseeker.manager;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;

/**
 * Created by Simon on 07.05.2015.
 */
public class GPSManager extends Service implements LocationListener {

    private LocationManager mLocationManager;
    private Location        mLocation;
    private Double          mLatitude;
    private Double          mLongitude;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;


    public GPSManager(Context context) {
        mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    public boolean setLocation() {
        boolean ret = false;
        if (isNetworkEnabled()) {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            if (mLocationManager != null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (mLocation != null) {
                mLatitude = mLocation.getLatitude();
                mLongitude = mLocation.getLongitude();
            }

        } else if (isGPSEnabled()) {
            if (mLocation == null){
                mLocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            }
            if (mLocationManager != null){
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (mLocation != null){
                    mLatitude = mLocation.getLatitude();
                    mLongitude = mLocation.getLongitude();
                }
            }
        }

        if (mLatitude != null && mLongitude != null) {
            ret = true;
        } else {
            ret = false;
        }
        return ret;
    }

    public boolean isNetworkEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public boolean isGPSEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void close() {
        if (mLocationManager != null){
            mLocationManager.removeUpdates(this);
        }
    }

    public Double getLatitude() {
        if (mLatitude != null) {
            return mLatitude;
        }
        return 0.;    }

    public Double getLongitude() {
        if (mLongitude != null) {
            return mLongitude;
        }
        return 0.;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
