﻿#region usings

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Web.Http;

#endregion

namespace SightSeeker_Universal.Common
{
    internal static class BackendResponseDecoder
    {
        private static readonly string _soapEnvNS = "http://schemas.xmlsoap.org/soap/envelope/";
        private static readonly string _sightseekerNS = "https://star-i13013.ba-horb.de:23443/sightseeker/server.php";

        static BackendResponseDecoder()
        {
            _soapEnvNS = "http://schemas.xmlsoap.org/soap/envelope/";
            _sightseekerNS = "https://star-i13013.ba-horb.de:23443/sightseeker/server.php";
        }

        internal static async Task<string> DecodeLoginResponse(HttpResponseMessage loginResponse)
        {
            string responseContent = await loginResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("loginResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement loginToken = returnValue.Element(XName.Get("result", ""));
                return loginToken.Value;
            }
            if (returnCode.Value == "19")
            {
                throw new LoginFailedException();
            }
            if (returnCode.Value == "21")
            {
                throw new LoginFailedException();
            }
            if (returnCode.Value == "23")
            {
                throw new DoubleOptInException();
            }
            if (returnCode.Value == "25")
            {
                throw new LoginFailedException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async void DecodeLogoutResponse(HttpResponseMessage logoutResponse)
        {
            string responseContent = await logoutResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("logoutResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                return;
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<string> DecodeGetInfoTextFromTagResponse(
            HttpResponseMessage getInfoTextFromTagResponse)
        {
            string responseContent = await getInfoTextFromTagResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getInfoTextFromTagIDResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement text = returnValue.Element(XName.Get("result", ""));
                return text.Value;
            }
            if (returnCode.Value == "29")
            {
                return "";
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<string> DecodeGetPictureLinkFromTagResponse(
            HttpResponseMessage getPictureLinkFromTagResponse)
        {
            string responseContent = await getPictureLinkFromTagResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getPictureLinkFromTagIDResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement text = returnValue.Element(XName.Get("result", ""));
                return text.Value;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            if (returnCode.Value == "33")
            {
                throw new NoPictureAvailableException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<string> DecodeGetUserScoreFromTokenResponse(
            HttpResponseMessage getUserScoreFromTokenResponse)
        {
            string responseContent = await getUserScoreFromTokenResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getUserScoreFromTokenResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement text = returnValue.Element(XName.Get("result", ""));
                return text.Value;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<List<UserScore>> DecodeGetGlobalHighscoreFromTokenResponse(
            HttpResponseMessage getGlobalHighscoreFromTokenResponse)
        {
            string responseContent = await getGlobalHighscoreFromTokenResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getGlobalHighscoreResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement highScores = returnValue.Element(XName.Get("result", _sightseekerNS));
                IEnumerable<XElement> items = highScores.Elements(XName.Get("item", _sightseekerNS));
                List<UserScore> result = new List<UserScore>();
                foreach (XElement item in items)
                {
                    UserScore score = new UserScore
                        (
                        int.Parse(item.Element(XName.Get("UserId", "")).Value),
                        int.Parse(item.Element(XName.Get("Score", "")).Value)
                        );
                    result.Add(score);
                }
                return result;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async void DecodeRegisterUserResponse(HttpResponseMessage registerUserResponse)
        {
            string responseContent = await registerUserResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("registerUserResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                return;
            }
            if (returnCode.Value == "13")
            {
                throw new UserAllreadyExistsException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async void DecodeUpdatePasswordResponse(HttpResponseMessage updatePasswordResponse)
        {
            string responseContent = await updatePasswordResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("updateUserPasswordResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                return;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async void DecodeVisitTagResponse(HttpResponseMessage visitTagResponse)
        {
            string responseContent = await visitTagResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("visitTagResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                return;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            if (returnCode.Value == "15")
            {
                throw new InvalidKoordinatesException();
            }

            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<List<AppTag>> DecodeGetVisitedTagsFromTokenResponse(string responseContent)
        {
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getVisitedTagsFromTokenResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement visitedTags = returnValue.Element(XName.Get("visitedTags", ""));
                IEnumerable<XElement> items = from el in visitedTags.Elements(XName.Get("item", "")) select el;
                List<AppTag> result = new List<AppTag>();
                foreach (XElement item in items)
                {
                    AppTag tag = new AppTag
                        (
                        item.Element(XName.Get("ID", "")).Value,
                        item.Element(XName.Get("Name", "")).Value,
                        int.Parse(item.Element(XName.Get("Score", "")).Value),
                        double.Parse(item.Element(XName.Get("Longitude", "")).Value, CultureInfo.InvariantCulture),
                        double.Parse(item.Element(XName.Get("Latitude", "")).Value, CultureInfo.InvariantCulture),
                        item.Element(XName.Get("VisitingDate", "")).Value
                        );
                    result.Add(tag);
                }
                return result;
            }
            if (returnCode.Value == "17")
            {
                return new List<AppTag>();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<List<AppTag>> DecodeGetNearTagsResponse(string responseContent)
        {
            XDocument document = XDocument.Parse(responseContent);
            List<AppTag> result = new List<AppTag>();
            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("getNearTagsResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                XElement nearTags = returnValue.Element(XName.Get("visitedTags", ""));
                IEnumerable<XElement> items = from el in nearTags.Elements(XName.Get("item", "")) select el;

                foreach (XElement item in items)
                {
                    AppTag tag = new AppTag
                        (
                        item.Element(XName.Get("ID", "")).Value,
                        item.Element(XName.Get("Name", "")).Value,
                        int.Parse(item.Element(XName.Get("Score", "")).Value),
                        float.Parse(item.Element(XName.Get("Longitude", "")).Value),
                        float.Parse(item.Element(XName.Get("Latitude", "")).Value),
                        float.Parse(item.Element(XName.Get("Distance", "")).Value)
                        );
                    result.Add(tag);
                }
                return result;
            }
            if (returnCode.Value == "31")
            {
                return result;
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }

            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }

        internal static async Task<int> DecodeCountVisitedTagsResponse(HttpResponseMessage countVisitedTagsResponse)
        {
            string responseContent = await countVisitedTagsResponse.Content.ReadAsStringAsync();
            XDocument document = XDocument.Parse(responseContent);

            XElement returnValue =
                document.Root.Element(XName.Get("Body", _soapEnvNS))
                    .Element(XName.Get("countVisitedTagsFromTokenResponse", _sightseekerNS))
                    .Element(XName.Get("return", ""));
            XElement returnCode = returnValue.Element(XName.Get("returnCode", ""));

            if (returnCode.Value == "0")
            {
                return int.Parse(returnValue.Element(XName.Get("result", "")).Value);
            }
            if (returnCode.Value == "9")
            {
                throw new InvalidTokenException();
            }
            throw new SightseekerServerException(int.Parse(returnCode.Value), "Something went wrong on the server.");
        }
    }
}
