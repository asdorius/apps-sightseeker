﻿#region usings

using Windows.UI.Xaml.Controls;

#endregion

// Die Elementvorlage "Inhaltsdialog" ist unter "http://go.microsoft.com/fwlink/?LinkID=390556" dokumentiert.

namespace SightSeeker_Universal
{
    public sealed partial class LoginDialog
    {
        public LoginDialog()
        {
            InitializeComponent();
        }

        public string EmailData { get; private set; }
        public string PasswordData { get; private set; }

        private async void loginClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            EmailData = email.Text;
            PasswordData = password.Password;
        }

        private void registerClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}