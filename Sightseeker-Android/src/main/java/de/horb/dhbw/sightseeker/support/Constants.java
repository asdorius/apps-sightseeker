package de.horb.dhbw.sightseeker.support;

import android.os.Environment;

/**
 * Created by Simon on 02.04.2015.
 */
public class Constants {

    // Return-Code
    public static final int RETURN_CODE_SUCCESSFULL             = 0;
    public static final int RETURN_CODE_UNKNOWN_ERROR           = -1;
    public static final int RETURN_CODE_CONNECTION_FAILED       = 5;
    public static final int RETURN_CODE_INVALID_TOKEN           = 7;
    public static final int RETURN_CODE_QUERY_FAILED            = 9;
    public static final int RETURN_CODE_FUNC_ULAT_FAILED        = 11;
    public static final int RETURN_CODE_USER_ALREADY_EXISTS     = 13;
    public static final int RETURN_CODE_INVALID_TAG_DATA        = 15;
    public static final int RETURN_CODE_NO_PLACES_FOUND         = 17;
    public static final int RETURN_CODE_INVALID_EMAIL_PASSWORD  = 21;
    public static final int RETURN_CODE_USER_NOT_VERIFIED       = 23;
    public static final int RETURN_CODE_USER_DELETED            = 25;
    public static final int RETURN_CODE_EXCEPTION               = -2;

    // ERROR Return-Code
    public static final String RC_ERROR_CONNECTION_FAILED       = "Keine Verbindung zur Datenbank möglich";
    public static final String RC_ERROR_QUERY_FAILED            = "Datenbankabfrage fehlgeschlagen";
    public static final String RC_ERROR_USER_ALREADY_EXISTS     = "Der Benutzer ist bereits vorhanden";
    public static final String RC_ERROR_INVALID_TAG_DATA        = "Die Koordinaten des besuchten Ortes sind ungültig";
    public static final String RC_ERROR_NO_PLACES_FOUND         = "Noch keine Orte registriert";
    public static final String RC_ERROR_INVALID_EMAIL_PASSWORD  = "E-Mail Adresse oder Passwort ist falsch";
    public static final String RC_ERROR_USER_NOT_VERIFIED       = "Benutzer ist nicht verifiziert";
    public static final String RC_ERROR_USER_DELETED            = "Benutzer wurde gelöscht";

    // Error
    public static final String ERROR_UNKNOWN_ERROR          = "Ein unbekannter Fehler ist aufgetreten";
    public static final String ERROR_SYNCHRONIZE_FAILED     = "Synchronisierung fehlgeschlagen";
    public static final String ERROR_NETWORK_FAILED         = "Internetverbindung fehlgeschlagen";
    public static final String ERROR_WRITE_PLACE            = "Ort konnte nicht synchronisiert werden";
    public static final String ERROR_LOADING_IMAGE          = "Bild konnte nicht geladen werden";
    public static final String ERROR_OFFLINE_MODE           = "Sie befinden sich im Offline-Modus";

    // Announcement
    public static final String ANNOUNCE_TAG_LOGIN_FIRST     = "Tag erkannt: Ort wird nach Anmeldung registriert";
    public static final String ANNOUNCE_TAG_NOT_ACTUAL_PLACE = "Aktueller Standort stimmt nicht mit Tag überein";
    public static final String ANNOUNCE_GPS_NOT_ACTIVATED   = "Standortbestimmung ausgeschaltet";
    public static final String ANNOUNCE_LOST_NFC_DATA       = "NFC-Daten verloren gegangen";
    public static final String ANNOUNCE_OFFLINE_MODE        = "Offline-Modus";
    public static final String ANNOUNCE_PASSWORD_CHANGED    = "Passwort wurde geändert";

    // Webservice
    public static final String  WEBSERVICE_ADRESS_IN    = "http://star-i13013.ba-horb.de/sightseeker/server.php";
    public static final String  WEBSERVICE_ADRESS       = "https://star-i13013.ba-horb.de:23443/sightseeker/server.php";
    public static final int     WEBSERVICE_TIMEOUT      = 20000;
    // WebserviceTask Methode-Code
    public static final int WEBSERVICE_TASK_LOGIN               = 100;
    public static final int WEBSERVICE_TASK_LOGOUT              = 102;
    public static final int WEBSERVICE_TASK_REGISTER_USER       = 104;
    public static final int WEBSERVICE_TASK_UPDATE_PASSWORD     = 106;
    public static final int WEBSERVICE_TASK_VISIT_TAG           = 108;
    public static final int WEBSERVICE_TASK_GET_DATABASE        = 110;
    public static final int WEBSERVICE_TASK_GET_PICTURE_LINK    = 112;

    // Database
    public static final String  DATABASE_NAME           = "sightseeker.db";
    public static final int     DATABASE_VERSION        = 1;
    public static final String  DATABASE_FILE           = Environment.getDataDirectory() + "/data/" + "@string/app_name" + "/databases/" + DATABASE_NAME;

    // Arguments
    public static final String ARG_INTENT_ACTION    = "intent_action";
    public static final String ARG_OFFLINE_MODE     = "offline_mode";
    public static final String ARG_SECTION_NUMBER   = "section_number";
    public static final String ARG_NFC_DATA         = "nfc_data";
    public static final String ARG_LOGGED_OUT       = "logged_out";
    public static final String ARG_PLACE_ID         = "place_id";

    // PlacesFragment
    public static final String PLACE_UNKNOWN_PLACE      = "Unbekannt";

    // Activity names and fragment names
    public static final String ACTIVITY_WELCOME     = "WelcomeActivity";
    public static final String ACTIVITY_LOGIN       = "LoginActivity";
    public static final String ACTIVIY_REGISTER     = "RegisterActivity";
    public static final String ACTIVITY_NAVIGATION  = "NavigationDrawerActivity";
    public static final String FRAGMENT_PROFILE     = "ProfileFragment";
    public static final String FRAGMENT_PLACES      = "PlacesFragment";
    public static final String FRAGMENT_SETTINGS    = "SettingsFragment";

    public static final String INTENT_ACTION_MAIN   = "android.intent.action.MAIN";
}
