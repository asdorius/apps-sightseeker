package de.horb.dhbw.sightseeker.webservice;

import org.apache.http.util.ByteArrayBuffer;

/**
 * Created by Simon on 08.05.2015.
 * This class represents the result from a database access.
 * It contains every class which can be retrieved by a database access.
 */
public class WebserviceTaskResult {

    private App_User_Result mAppUserResult;
    private App_Tag_Result mAppTagResult;
    private App_UserTags_Result mAppUserTagsResult;
    private String_Result mStringResult;
    private ReturnCode_Result mReturnCodeResult;
    private Int_Result mIntResult;
    private ByteArrayBuffer mImageByteArrayBuffer;
    private Integer mTagId;

    public App_User_Result getAppUserResult() {
        return mAppUserResult;
    }

    public void setAppUserResult(App_User_Result appUserResult) {
        mAppUserResult = appUserResult;
    }

    public App_Tag_Result getAppTagResult() {
        return mAppTagResult;
    }

    public void setAppTagResult(App_Tag_Result appTagResult) {
        mAppTagResult = appTagResult;
    }

    public App_UserTags_Result getAppUserTagsResult() {
        return mAppUserTagsResult;
    }

    public void setAppUserTagsResult(App_UserTags_Result appUserTagsResult) {
        mAppUserTagsResult = appUserTagsResult;
    }

    public ReturnCode_Result getReturnCodeResult() {
        return mReturnCodeResult;
    }

    public void setReturnCodeResult(ReturnCode_Result returnCodeResult) {
        this.mReturnCodeResult = returnCodeResult;
    }

    public String_Result getStringResult() {
        return mStringResult;
    }

    public void setStringResult(String_Result stringResult) {
        this.mStringResult = stringResult;
    }

    public Int_Result getIntResult() {
        return mIntResult;
    }

    public void setIntResult(Int_Result intResult) {
        this.mIntResult = intResult;
    }

    public ByteArrayBuffer getImageByteArrayBuffer() {
        return mImageByteArrayBuffer;
    }

    public void setImageByteArrayBuffer(ByteArrayBuffer imageByteArrayBuffer) {
        this.mImageByteArrayBuffer = imageByteArrayBuffer;
    }

    public Integer getTagId() {
        return mTagId;
    }

    public void setTagId(Integer tagId) {
        this.mTagId = tagId;
    }
}
