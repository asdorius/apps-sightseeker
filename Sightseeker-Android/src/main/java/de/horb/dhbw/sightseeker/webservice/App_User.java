package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class App_User extends SoapObject {
    private java.lang.Integer ID;

    private java.lang.String EMail;

    private java.lang.String username;

    public App_User() {
        super("", "");
    }
    public void setID(java.lang.Integer ID) {
        this.ID = ID;
    }

    public java.lang.Integer getID() {
        return this.ID;
    }

    public void setEMail(java.lang.String EMail) {
        this.EMail = EMail;
    }

    public java.lang.String getEMail() {
        return this.EMail;
    }

    public void setUsername(java.lang.String username) {
        this.username = username;
    }

    public java.lang.String getUsername() {
        return this.username;
    }

    public int getPropertyCount() {
        return 3;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return ID;
        case 1: return EMail;
        case 2: return username;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: ID = (java.lang.Integer) __obj; break;
        case 1: EMail = (java.lang.String) __obj; break;
        case 2: username = (java.lang.String) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "ID";
            __info.type = java.lang.Integer.class; break;
        case 1:
            __info.name = "EMail";
            __info.type = java.lang.String.class; break;
        case 2:
            __info.name = "username";
            __info.type = java.lang.String.class; break;
        }
    }

}
