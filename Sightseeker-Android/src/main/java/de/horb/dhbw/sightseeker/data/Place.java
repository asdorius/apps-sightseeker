package de.horb.dhbw.sightseeker.data;


import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.webservice.App_Tag;

/**
 * Created by Simon on 09.04.2015.
 * Represents the datatype for a record in the table "place"
 */
public class Place {

    private Long        mId;
    private Integer     mTagId;
    private String      mPlace;
    private Integer     mPoints;
    private Double      mLatitude;
    private Double      mLongitude;
    private String      mDateTime;
    private Integer     mUserId;
    private int         mSynced;

    public Place () {
    }

    public Place (String place, Double latitude, Double longitude, String dateTime) {
        setPlace(place);
        setLatitude(latitude);
        setLongitude(longitude);
        setDateTime(dateTime);
    }

    public static Place createPlaceByAppTag(App_Tag appTag) {
        if (appTag != null) {
            Place place = new Place();
            place.setTagId(appTag.getID());
            place.setDateTime(appTag.getVisitingDate());
            place.setPlace(appTag.getName());
            place.setPoints(appTag.getScore());
            place.setLatitude(appTag.getLatitude());
            place.setLongitude(appTag.getLongitude());
            return place;
        }
        return null;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        this.mId = id;
    }

    public String getPlace() {
        if (mPlace != null) {
            return mPlace;
        } else {
            return Constants.PLACE_UNKNOWN_PLACE;
        }
    }

    public void setPlace(String place) {
        this.mPlace = place;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        this.mDateTime = dateTime;
    }

    public Double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(Double latitude) {
        this.mLatitude = latitude;
    }

    public Double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(Double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public Integer getPoints() {
        if (mPoints != null) {
            return mPoints;
        } else {
            return 0;
        }
    }

    public void setPoints(Integer points) {
        this.mPoints = points;
    }

    public Integer getTagId() {
        return mTagId;
    }

    public void setTagId(Integer tagId) {
        this.mTagId = tagId;
    }

    public Integer getUserId() {
        return mUserId;
    }

    public void setUserId(Integer userId) {
        this.mUserId = userId;
    }

    public int getSynced() {
        return mSynced;
    }

    public void setSynced(int synced) {
        this.mSynced = synced;
    }
}
