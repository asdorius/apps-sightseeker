﻿public class AppTag
{
    public AppTag(string id, string name, int score, double longitude, double latitude, string date)
    {
        ID = id;
        Name = name;
        Score = score;
        Longitude = longitude;
        Latitude = latitude;
        VisitingDate = date;
    }

    public AppTag(string id, string name, int score, double longitude, double latitude, double distance)
    {
        ID = id;
        Name = name;
        Score = score;
        Longitude = longitude;
        Latitude = latitude;
        Distance = distance;
    }


    public AppTag()
    {
    }

    public string ID { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
    public double Longitude { get; set; }
    public double Latitude { get; set; }
    public double Distance { get; set; }
    public string VisitingDate { get; set; }
    public string ImageLink { get; set; }
    public string Description { get; set; }
}