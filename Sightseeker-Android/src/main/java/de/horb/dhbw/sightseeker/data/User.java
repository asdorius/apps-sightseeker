package de.horb.dhbw.sightseeker.data;

import de.horb.dhbw.sightseeker.webservice.App_User;

/**
 * Created by Simon on 12.05.2015.
 * Represents the datatype for a record in the table "user"
 */
public class User {

    private long    mId;
    private String  mUsername;
    private String  mEmail;
    private String  mPassword;
    private Integer mCountVisited;
    private String  mLastVisited;
    private Integer mLastTagId;
    private Integer mTotalPoints;
    private int     mSynced;


    public static User createUserByAppUser(App_User appUser) {
        User user = new User();
        user.setId(appUser.getID());
        user.setUsername(appUser.getUsername());
        user.setEmail(appUser.getEMail());

        return user;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public Integer getCountVisited() {
        return mCountVisited;
    }

    public void setCountVisited(Integer countVisited) {
        this.mCountVisited = countVisited;
    }

    public String getLastVisited() {
        return mLastVisited;
    }

    public void setLastVisited(String lastVisited) {
        this.mLastVisited = lastVisited;
    }

    public Integer getTotalPoints() {
        return mTotalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.mTotalPoints = totalPoints;
    }

    public Integer getLastTagId() {
        return mLastTagId;
    }

    public void setLastTagId(Integer lastTagId) {
        this.mLastTagId = lastTagId;
    }

    public int getSynced() {
        return mSynced;
    }

    public void setSynced(int synced) {
        this.mSynced = synced;
    }
}
