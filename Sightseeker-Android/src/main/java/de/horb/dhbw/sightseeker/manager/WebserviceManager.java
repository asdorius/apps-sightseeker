package de.horb.dhbw.sightseeker.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import de.horb.dhbw.sightseeker.activity.LoginActivity;
import de.horb.dhbw.sightseeker.activity.NavigationDrawerActivity;
import de.horb.dhbw.sightseeker.activity.RegisterActivity;
import de.horb.dhbw.sightseeker.activity.WelcomeActivity;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.database.Synchronizer;
import de.horb.dhbw.sightseeker.fragment.PlacesFragment;
import de.horb.dhbw.sightseeker.fragment.SettingsFragment;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.webservice.App_Tag_Result;
import de.horb.dhbw.sightseeker.webservice.App_UserTags_Result;
import de.horb.dhbw.sightseeker.webservice.App_User_Result;
import de.horb.dhbw.sightseeker.webservice.Configuration;
import de.horb.dhbw.sightseeker.webservice.ReturnCode_Result;
import de.horb.dhbw.sightseeker.webservice.SightSeekerServicesPort;
import de.horb.dhbw.sightseeker.webservice.String_Result;
import de.horb.dhbw.sightseeker.webservice.WebserviceTaskResult;

/**
 * Created by Simon on 08.05.2015.
 *
 * This class serves as an interface for the various accesses to the database.
 * Each constructor requires a value that defines the type of access (mMethod).
 * Each constructor requires a context for the callback after accessing the database.
 */
public class WebserviceManager {

    public static String token;

    private int mMethod;
    private WebserviceTask mWebserviceTask;
    private WebserviceTaskResult mWebserviceTaskResult;
    private Context mContext;
    private Synchronizer mSynchronizer;

    private String  mEmail;
    private String  mPassword;
    private String  mUsername;
    private String  mNewPassword;
    private Double  mLatitude;
    private Double  mLongitude;
    private String  mDatetime;
    private int     mOffset;
    private int     mLimit;
    private Integer mTagId;


    // Constructor for method "logout"
    public WebserviceManager(Context context, int method) {
        mContext = context;
        mMethod = method;
    }

    // Constructor for method "login"
    public WebserviceManager(Context context, int method, String email, String password) {
        mContext = context;
        mMethod = method;
        mEmail = email;
        mPassword = password;
    }

    // Constructor for method "register"
    public WebserviceManager(Context context, int method, String email, String username, String password) {
        mContext = context;
        mMethod = method;
        mEmail = email;
        mUsername = username;
        mPassword = password;
    }

    // Constructor for method "updatePassword"
    public WebserviceManager(Context context, int method, String newPassword) {
        mContext = context;
        mMethod = method;
        mNewPassword = newPassword;
    }

    // Constructor for method "visitTag"
    public WebserviceManager(Context context, int method, Double latitude, Double longitude, String datetime) {
        mContext = context;
        mMethod = method;
        mLatitude = latitude;
        mLongitude = longitude;
        mDatetime = datetime;
    }

    // Constructor for method "visitTag" by class "Synchronizer"
    public WebserviceManager(Synchronizer synchronizer, Context context,
                             int method, Double latitude, Double longitude, String datetime) {
        mSynchronizer = synchronizer;
        mContext = context;
        mMethod = method;
        mLatitude = latitude;
        mLongitude = longitude;
        mDatetime = datetime;
    }

    // Constructor for method "getDatabase" by class "Synchronizer"
    public WebserviceManager(Synchronizer synchronizer, Context context, int method, int offset, int limit) {
        mSynchronizer = synchronizer;
        mContext = context;
        mMethod = method;
        mOffset = offset;
        mLimit = limit;
    }

    // Constructor for nfc scan (method "getPictureLink")
    public WebserviceManager(Context context, int method, Integer tagId) {
        mContext = context;
        mMethod = method;
        mTagId = tagId;
    }

    // Constructor for method "getPictureLink" by class "Synchronizer"
    public WebserviceManager(Synchronizer synchronizer, Context context, int method, Integer tagId) {
        mSynchronizer = synchronizer;
        mContext = context;
        mMethod = method;
        mTagId = tagId;
    }

    // Returns an instance of "SightseekerServicesPort" on which is to make the access to the database
    public static SightSeekerServicesPort getServicesPort() {
        Configuration.setConfiguration(Constants.WEBSERVICE_ADRESS);
        SightSeekerServicesPort servicesPort = new SightSeekerServicesPort();
        return servicesPort;
    }

    // Runs the asynchronous task (access to database)
    public void doAccess() {
        if (mWebserviceTask != null) {
            return;
        }

        // If the token is null and the method doesn't require a token print error "offline mode"
        if (mMethod != Constants.WEBSERVICE_TASK_LOGIN && mMethod != Constants.WEBSERVICE_TASK_LOGOUT &&
                mMethod != Constants.WEBSERVICE_TASK_REGISTER_USER && token == null) {
            Toast.makeText(mContext, Constants.ERROR_OFFLINE_MODE, Toast.LENGTH_SHORT).show();
            return;
        }

        mWebserviceTask = new WebserviceTask();

        try {
            mWebserviceTask.execute((Void) null);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mContext, Constants.ERROR_UNKNOWN_ERROR, Toast.LENGTH_SHORT).show();
        }
    }

    // Returns true if the parameter passed is 0
    public static boolean accessSuccessfull(int returnCode) {
        return returnCode == Constants.RETURN_CODE_SUCCESSFULL;
    }

    // Toast an error by switching the value of the "returnCode"
    public static void toastErrorByReturnCode(Context context, int returnCode) {
        switch (returnCode) {
            case Constants.RETURN_CODE_UNKNOWN_ERROR:
                Toast.makeText(context, Constants.ERROR_UNKNOWN_ERROR, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_CONNECTION_FAILED:
                Toast.makeText(context, Constants.RC_ERROR_CONNECTION_FAILED, Toast.LENGTH_SHORT);
                break;
            case Constants.RETURN_CODE_INVALID_TOKEN:
                Toast.makeText(context, Constants.ERROR_OFFLINE_MODE, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_QUERY_FAILED:
                Toast.makeText(context, Constants.RC_ERROR_QUERY_FAILED, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_USER_ALREADY_EXISTS:
                Toast.makeText(context, Constants.RC_ERROR_USER_ALREADY_EXISTS, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_INVALID_TAG_DATA:
                Toast.makeText(context, Constants.RC_ERROR_INVALID_TAG_DATA, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_NO_PLACES_FOUND:
                Toast.makeText(context, Constants.RC_ERROR_NO_PLACES_FOUND, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_INVALID_EMAIL_PASSWORD:
                Toast.makeText(context, Constants.RC_ERROR_INVALID_EMAIL_PASSWORD, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_USER_NOT_VERIFIED:
                Toast.makeText(context, Constants.RC_ERROR_USER_NOT_VERIFIED, Toast.LENGTH_SHORT).show();
                break;
            case Constants.RETURN_CODE_USER_DELETED:
                Toast.makeText(context, Constants.RC_ERROR_USER_DELETED, Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, Constants.ERROR_UNKNOWN_ERROR, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    // Handle "AsyncTaskResult" for different methods and call the specific method in this class
    public void handleTaskResult() {
        switch (mMethod) {
            case Constants.WEBSERVICE_TASK_LOGIN:
                handleLogin();
                break;
            case Constants.WEBSERVICE_TASK_LOGOUT:
                handleLogout();
                break;
            case Constants.WEBSERVICE_TASK_REGISTER_USER:
                handleRegisterUser();
                break;
            case Constants.WEBSERVICE_TASK_UPDATE_PASSWORD:
                handleUpdatePassword();
                break;
            case Constants.WEBSERVICE_TASK_VISIT_TAG:
                handleVisitTag();
                break;
            case Constants.WEBSERVICE_TASK_GET_DATABASE:
                handleGetDatabase();
                break;
            case Constants.WEBSERVICE_TASK_GET_PICTURE_LINK:
                handleGetPictureLink();
                break;
        }
    }

    // Handle the "login" result and call the specific method on the initial activity by switching context
    private void handleLogin() {
        switch (mContext.getClass().getSimpleName()) {
            case Constants.ACTIVITY_WELCOME:
                WelcomeActivity welcomeActivity = (WelcomeActivity) mContext;
                if (welcomeActivity != null) {
                    welcomeActivity.handleLogin(mWebserviceTaskResult);
                }
                break;
            case Constants.ACTIVITY_LOGIN:
                LoginActivity loginActivity = (LoginActivity) mContext;
                if (loginActivity != null) {
                    loginActivity.handleLogin(mWebserviceTaskResult);
                }
                break;
        }
    }

    // Handle the "logout" result: Call the specific method on the activity "NavigationDrawerActivity"
    private void handleLogout() {
        NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity) mContext;

        if (navigationDrawerActivity != null) {
            navigationDrawerActivity.handleAccessLogout();
        }
    }

    // Handle the "register user" result: Call the specific method on the activity "RegisterActivity"
    private void handleRegisterUser() {
        RegisterActivity registerActivity = (RegisterActivity) mContext;

        if (registerActivity != null) {
            registerActivity.handleRegisterUser(mWebserviceTaskResult);
        }
    }

    // Handle the "update password" result: Call the specific method on the fragment "SettingsFragment"
    private void handleUpdatePassword() {
        NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity) mContext;
        FragmentManager manager = navigationDrawerActivity.getSupportFragmentManager();
        SettingsFragment settingsFragment = (SettingsFragment) manager.findFragmentById(R.id.container);

        if (settingsFragment != null) {
            settingsFragment.handleUpdatePassword(mWebserviceTaskResult);
        }
    }

    // Handle the "visit tag" result: Call the specific method on the class "Synchronizer" or on the fragment "PlacesFragment"
    private void handleVisitTag() {
        NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity) mContext;
        FragmentManager manager = navigationDrawerActivity.getSupportFragmentManager();
        PlacesFragment placesFragment = (PlacesFragment) manager.findFragmentById(R.id.container);

        if (mSynchronizer != null) {
            mSynchronizer.handleVisitTag(mWebserviceTaskResult);
        } else if (placesFragment != null) {
            placesFragment.handleVisitTag(mWebserviceTaskResult);
        }
    }

    // Handle the "get database" result: Call the specific method on the class "Synchronizer"
    private void handleGetDatabase() {
        if (mSynchronizer != null) {
            mSynchronizer.handleGetDatabase(mWebserviceTaskResult);
        }
    }

    // Handle the "get picture link" result: Call the specific method on the class "Synchronizer" or on the fragment "PlacesFragment"
    private void handleGetPictureLink() {
        if (mSynchronizer != null) {
            mSynchronizer.handleGetPictureLink(mWebserviceTaskResult);
        } else {
            NavigationDrawerActivity activity = (NavigationDrawerActivity) mContext;
            FragmentManager manager = activity.getSupportFragmentManager();
            PlacesFragment placesFragment = (PlacesFragment) manager.findFragmentById(R.id.container);
            if (placesFragment != null) {
                placesFragment.handleGetPictureLink(mWebserviceTaskResult);
            }
        }
    }

    // Asynchronous task which handles the various access to the database by switching "mMethod" value
    public class WebserviceTask extends AsyncTask<Void, Void, Void> {

        private SightSeekerServicesPort mServicesPort;

        // Constructor for methods "logout", "getFirstUser" and "getVisitedTags"
        WebserviceTask () {
            mServicesPort = getServicesPort();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Show progress bar
            showProgress(true);
        }

        @Override
        protected Void doInBackground(Void...params) {
            // Create a "WebserviceTaskResult" which saves the retrieved data from the database
            mWebserviceTaskResult = new WebserviceTaskResult();

            // Call the specific method by switching "mMethod" value
            switch (mMethod) {
                case Constants.WEBSERVICE_TASK_LOGIN:
                    login();
                    break;
                case Constants.WEBSERVICE_TASK_REGISTER_USER:
                    register();
                    break;
                case Constants.WEBSERVICE_TASK_LOGOUT:
                    logout();
                    break;
                case Constants.WEBSERVICE_TASK_UPDATE_PASSWORD:
                    updatePassword();
                    break;
                case Constants.WEBSERVICE_TASK_GET_DATABASE:
                    getDatabase();
                    break;
                case Constants.WEBSERVICE_TASK_VISIT_TAG:
                    visitTag();
                    break;
                case Constants.WEBSERVICE_TASK_GET_PICTURE_LINK:
                    getPictureLink();
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            mWebserviceTask = null;
            // Hide progress bar
            showProgress(false);
            handleTaskResult();
        }

        @Override
        protected void onCancelled() {
            mWebserviceTask = null;
            // Hide progress bar
            showProgress(false);
            handleTaskResult();
        }

        // Try to log in by email and password
        private void login() {
            String_Result stringResult = null;
            try {
                stringResult = mServicesPort.login(mEmail, mPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setStringResult(stringResult);
        }

        // Try to register by email, username and password
        private void register() {
            ReturnCode_Result returnCodeResult = null;
            try {
                returnCodeResult = mServicesPort.registerUser(mEmail, mUsername, mPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setReturnCodeResult(returnCodeResult);
        }

        // Try to log out by token
        private void logout() {
            ReturnCode_Result returnCodeResult = null;
            try {
                returnCodeResult = mServicesPort.logout(token);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setReturnCodeResult(returnCodeResult);
        }

        // Try to update password for user by member "mToken"
        private void updatePassword() {
            ReturnCode_Result returnCodeResult = null;
            try {
                returnCodeResult = mServicesPort.updateUserPassword(token, mNewPassword);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setReturnCodeResult(returnCodeResult);
        }

        // Try to create tag record in database by token latitude and longitude
        private void visitTag() {
            App_Tag_Result appTagResult = null;
            try {
                appTagResult = mServicesPort.visitTag(token, mLongitude, mLatitude, mDatetime);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setAppTagResult(appTagResult);
        }

        // Try to get the user and all visited tags by token (only called by the class "Synchronizer")
        private void getDatabase() {
            App_UserTags_Result appUserTagsResult   = null;
            App_User_Result     appUserResult       = null;
            try {
                appUserTagsResult = mServicesPort.getVisitedTagsFromToken(token, mOffset, mLimit);
                appUserResult = mServicesPort.getUserFromToken(token);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setAppUserTagsResult(appUserTagsResult);
            mWebserviceTaskResult.setAppUserResult(appUserResult);
        }

        // Try to get the picture link by the tagId representing a place in database
        private void getPictureLink() {
            String_Result stringResult;
            ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(DatabaseHandler.DATABASE_BYTE_SIZE);
            try {
                stringResult = mServicesPort.getPictureLinkFromTagID(token, mTagId);

                // Try to fetch the picture by the retrieved link if the access was successfull
                if (WebserviceManager.accessSuccessfull(stringResult.getReturnCode()) &&
                        stringResult.getResult() != null) {
                    URL url = new URL(stringResult.getResult());
                    URLConnection urlConnection = url.openConnection();
                    InputStream inputStream = urlConnection.getInputStream();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, DatabaseHandler.DATABASE_BYTE_SIZE);
                    byteArrayBuffer = new ByteArrayBuffer(DatabaseHandler.DATABASE_BYTE_SIZE);
                    int current;
                    while ((current = bufferedInputStream.read()) != -1) {
                        byteArrayBuffer.append((byte) current);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Save the retrieved result in "WebserviceTaskResult"
            mWebserviceTaskResult.setImageByteArrayBuffer(byteArrayBuffer);
            mWebserviceTaskResult.setTagId(mTagId);
        }

        // Calls the method "showProgress" in different classes by switching the initial context
        private void showProgress(boolean show) {
            switch (mContext.getClass().getSimpleName()) {
                case Constants.ACTIVITY_WELCOME:
                    WelcomeActivity welcomeActivity = (WelcomeActivity) mContext;
                    if (welcomeActivity != null) {
                        welcomeActivity.showProgress(show,true);
                    }
                    break;
                case Constants.ACTIVITY_LOGIN:
                    LoginActivity loginActivity = (LoginActivity) mContext;
                    if (loginActivity != null) {
                        loginActivity.showProgress(show);
                    }
                    break;
                case Constants.ACTIVIY_REGISTER:
                    RegisterActivity registerActivity = (RegisterActivity) mContext;
                    if (registerActivity != null) {
                        registerActivity.showProgress(show);
                    }
                    break;
                case Constants.ACTIVITY_NAVIGATION:
                    NavigationDrawerActivity activity = (NavigationDrawerActivity) mContext;
                    FragmentManager manager = activity.getSupportFragmentManager();

                    switch (manager.findFragmentById(R.id.container).getClass().getSimpleName()) {
                        case Constants.FRAGMENT_SETTINGS:
                            SettingsFragment settingsFragment = (SettingsFragment) manager.findFragmentById(R.id.container);
                            if (settingsFragment != null) {
                                settingsFragment.showProgress(show);
                            }
                            break;
                        default:
                            return;
                    }
                    break;
                default:
                    return;
            }
        }
    }
}
