﻿namespace SightSeeker_Universal.DataModel
{
    public class User
    {
        public User(string email, string username, string token, string password)
        {
            Email = email;
            UserName = username;
            Token = token;
            Password = password;
        }

        public string Email { get; private set; }
        public string Token { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string Score { get; private set; }
    }
}