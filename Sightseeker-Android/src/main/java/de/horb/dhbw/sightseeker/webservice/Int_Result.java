package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class Int_Result extends SoapObject {
    private java.lang.Integer returnCode;

    private java.lang.Integer result;

    public Int_Result() {
        super("", "");
    }
    public void setReturnCode(java.lang.Integer returnCode) {
        this.returnCode = returnCode;
    }

    public java.lang.Integer getReturnCode() {
        return this.returnCode;
    }

    public void setResult(java.lang.Integer result) {
        this.result = result;
    }

    public java.lang.Integer getResult() {
        return this.result;
    }

    public int getPropertyCount() {
        return 2;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return returnCode;
        case 1: return result;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: returnCode = (java.lang.Integer) __obj; break;
        case 1: result = (java.lang.Integer) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "returnCode";
            __info.type = java.lang.Integer.class; break;
        case 1:
            __info.name = "result";
            __info.type = java.lang.Integer.class; break;
        }
    }

}
