package de.horb.dhbw.sightseeker.webservice;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

public final class App_Tag extends SoapObject {
    private java.lang.Integer ID;

    private java.lang.String name;

    private java.lang.Integer score;

    private java.lang.Double longitude;

    private java.lang.Double latitude;

    private java.lang.Double distance;

    private java.lang.String visitingDate;

    public App_Tag() {
        super("", "");
    }
    public void setID(java.lang.Integer ID) {
        this.ID = ID;
    }

    public java.lang.Integer getID() {
        return this.ID;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getName() {
        return this.name;
    }

    public void setScore(java.lang.Integer score) {
        this.score = score;
    }

    public java.lang.Integer getScore() {
        return this.score;
    }

    public void setLongitude(java.lang.Double longitude) {
        this.longitude = longitude;
    }

    public java.lang.Double getLongitude() {
        return this.longitude;
    }

    public void setLatitude(java.lang.Double latitude) {
        this.latitude = latitude;
    }

    public java.lang.Double getLatitude() {
        return this.latitude;
    }

    public void setDistance(java.lang.Double distance) {
        this.distance = distance;
    }

    public java.lang.Double getDistance() {
        return this.distance;
    }

    public void setVisitingDate(java.lang.String visitingDate) {
        this.visitingDate = visitingDate;
    }

    public java.lang.String getVisitingDate() {
        return this.visitingDate;
    }

    public int getPropertyCount() {
        return 7;
    }

    public Object getProperty(int __index) {
        switch(__index)  {
        case 0: return ID;
        case 1: return name;
        case 2: return score;
        case 3: return longitude;
        case 4: return latitude;
        case 5: return distance;
        case 6: return visitingDate;
        }
        return null;
    }

    public void setProperty(int __index, Object __obj) {
        switch(__index)  {
        case 0: ID = (java.lang.Integer) __obj; break;
        case 1: name = (java.lang.String) __obj; break;
        case 2: score = (java.lang.Integer) __obj; break;
        case 3: longitude = (java.lang.Double) __obj; break;
        case 4: latitude = (java.lang.Double) __obj; break;
        case 5: distance = (java.lang.Double) __obj; break;
        case 6: visitingDate = (java.lang.String) __obj; break;
        }
    }

    public void getPropertyInfo(int __index, Hashtable __table, PropertyInfo __info) {
        switch(__index)  {
        case 0:
            __info.name = "ID";
            __info.type = java.lang.Integer.class; break;
        case 1:
            __info.name = "name";
            __info.type = java.lang.String.class; break;
        case 2:
            __info.name = "score";
            __info.type = java.lang.Integer.class; break;
        case 3:
            __info.name = "longitude";
            __info.type = java.lang.Double.class; break;
        case 4:
            __info.name = "latitude";
            __info.type = java.lang.Double.class; break;
        case 5:
            __info.name = "distance";
            __info.type = java.lang.Double.class; break;
        case 6:
            __info.name = "visitingDate";
            __info.type = java.lang.String.class; break;
        }
    }

}
