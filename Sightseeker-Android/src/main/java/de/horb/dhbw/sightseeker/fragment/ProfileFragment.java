package de.horb.dhbw.sightseeker.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simon.sightseeker.R;

import de.horb.dhbw.sightseeker.activity.NavigationDrawerActivity;
import de.horb.dhbw.sightseeker.data.Image;
import de.horb.dhbw.sightseeker.data.Place;
import de.horb.dhbw.sightseeker.data.User;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.support.Constants;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    private User    mUser;
    private Image   mImage;

    private TextView mUsernameView;
    private TextView mEmailView;
    private TextView mCountVisitedView;
    private TextView mLastVisitedView;
    private TextView mTotalPointsView;
    private ImageView mImageView;

    private View     mProgressView;
    private View     mFormView;

    private static final String UNKNOWN = "Unbekannt";

    public static ProfileFragment newInstance(int sectionNumber) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Don't fetch data from database if nfc data was discovered
        if (!getParentActivity().nfcAction) {
            DatabaseHandler databaseHandler = new DatabaseHandler(getActivity());
            databaseHandler.openDataBase();
            // Get the user logged in
            mUser = databaseHandler.getUser();
            // Get the last visited place
            Place place = databaseHandler.getPlaceByTagId(mUser.getLastTagId());
            if (place != null) {
                // Get the image for the last visited place
                mImage = databaseHandler.getImageByTagId(place.getTagId());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        mUsernameView = (TextView) rootView.findViewById(R.id.username_textView);
        mEmailView = (TextView) rootView.findViewById(R.id.email_textView);
        mCountVisitedView = (TextView) rootView.findViewById(R.id.countVisited_textView);
        mLastVisitedView = (TextView) rootView.findViewById(R.id.lastVisited_textView);
        mTotalPointsView = (TextView) rootView.findViewById(R.id.totalPoints_textView);
        mImageView = (ImageView) rootView.findViewById(R.id.profile_imageView_lastVisited);
        mProgressView = rootView.findViewById(R.id.progressBar_profile);
        mFormView = rootView.findViewById(R.id.layout_profile);

        // Don't set data to views if nfc data was discovered
        if (!getParentActivity().nfcAction) {
            if (mUser.getUsername() != null) {
                mUsernameView.setText(mUser.getUsername());
            } else {
                mUsernameView.setText(UNKNOWN);
            }
            mEmailView.setText(mUser.getEmail());
            mCountVisitedView.setText(Integer.toString(mUser.getCountVisited()));
            if (mUser.getLastVisited() != null) {
                mLastVisitedView.setText(mUser.getLastVisited());
            } else {
                mLastVisitedView.setText(UNKNOWN);
            }
            mTotalPointsView.setText(Integer.toString(mUser.getTotalPoints()));
            if (mImage != null) {
                mImageView.setImageBitmap(BitmapFactory.decodeByteArray(mImage.getImageByteArray(),
                        0, mImage.getImageByteArray().length));
            }
        }

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((NavigationDrawerActivity) activity).onSectionAttached(getArguments().getInt(Constants.ARG_SECTION_NUMBER));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragment contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragment/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private NavigationDrawerActivity getParentActivity() {
        NavigationDrawerActivity navigationDrawerActivity = (NavigationDrawerActivity) getActivity();
        return navigationDrawerActivity;
    }

    public void refresh() {
        // TODO refresh view

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}