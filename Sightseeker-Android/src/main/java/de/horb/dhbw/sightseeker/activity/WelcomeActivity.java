package de.horb.dhbw.sightseeker.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.simon.sightseeker.R;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import de.horb.dhbw.sightseeker.data.User;
import de.horb.dhbw.sightseeker.database.DatabaseHandler;
import de.horb.dhbw.sightseeker.manager.WebserviceManager;
import de.horb.dhbw.sightseeker.support.Constants;
import de.horb.dhbw.sightseeker.support.Support;
import de.horb.dhbw.sightseeker.webservice.WebserviceTaskResult;


public class WelcomeActivity extends Activity {

    private LoadCertificate mLoadCertificateTask = null;
    private View mProgressView;

    private Button mButtonLogin;
    private Button mButtonRegister;

    private User mUser;

    private boolean mOfflineMode = false;
    private boolean mLoggedOut = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mProgressView = findViewById(R.id.welcome_progress);
        mButtonLogin = (Button) findViewById(R.id.welcome_buttonLogin);
        mButtonRegister = (Button) findViewById(R.id.welcome_buttonRegister);

        setButtons();
        // Hide buttons
        showProgress(false, true);

        // Check if activity is started by start-up or by logout
        if (!getIntent().hasExtra(Constants.ARG_LOGGED_OUT)) {
            loadCertificate();

            if (existsUserData()) {
                evaluateStart();
            } else {
                // Show the buttons for "login" and "register if no user data exist
                showProgress(false, false);
                // Show announcement if no user data exist but a tag was scanned
                if (isNFCAction()) {
                    Toast.makeText(this, Constants.ANNOUNCE_TAG_LOGIN_FIRST, Toast.LENGTH_LONG).show();
                }
            }
        } else {
            // Show buttons if logged out
            showProgress(false, false);
            mLoggedOut = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // TODO: Not needed ?
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO: Not needed ?
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Puts a bundle of data to the new intent
    @Override
    public void startActivity(Intent intent) {
        Bundle bundle = new Bundle();
        // Put the standard intent action to bundle if the user logged out
        // Otherwise get the original action of the intent
        if (!mLoggedOut) {
            bundle.putString(Constants.ARG_INTENT_ACTION, getIntent().getAction());
        } else {
            bundle.putString(Constants.ARG_INTENT_ACTION, Constants.INTENT_ACTION_MAIN);
        }
        // Put the value for offline mode to the bundle
        bundle.putBoolean(Constants.ARG_OFFLINE_MODE, mOfflineMode);
        intent.putExtras(bundle);
        intent.setData(getIntent().getData());
        super.startActivity(intent);
    }

    // Asynchronously load the certificate
    private void loadCertificate() {
        if (mLoadCertificateTask != null) {
            return;
        }
        mLoadCertificateTask = new LoadCertificate();
        mLoadCertificateTask.execute((Void) null);
    }

    // Checks if user exists in local database
    private boolean existsUserData() {
        boolean ret = false;
        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        databaseHandler.openDataBase();
        List<User> users = databaseHandler.getAllUsers();

        if (users.size() > 0) {
            mUser = databaseHandler.getFirstUser();
            ret = true;
        }
        return ret;
    }

    // Try to login if network is available, otherwise start in offline mode
    private void evaluateStart() {
        if (Support.isNetworkAvailable(this)){
            login();
            // TODO: Synchronize places on start up
        } else {
            Toast.makeText(this, Constants.ANNOUNCE_OFFLINE_MODE, Toast.LENGTH_SHORT).show();
            offlineStart();
        }
    }

    // Create an "WebserviceManager" with method value "login"
    // Try to login by email and password retrieved from local database
    private void login() {
        WebserviceManager webserviceManager = new WebserviceManager(this,
                Constants.WEBSERVICE_TASK_LOGIN, mUser.getEmail(), mUser.getPassword());
        webserviceManager.doAccess();
    }

    // Handles the database access of "login" by a "WebserviceTaskResult"
    public void handleLogin(WebserviceTaskResult taskResult) {
        if (taskResult.getStringResult() != null) {
            int returnCode = taskResult.getStringResult().getReturnCode();
            if (WebserviceManager.accessSuccessfull(returnCode)) {
                WebserviceManager.token = taskResult.getStringResult().getResult();
                startActivity(new Intent(this, NavigationDrawerActivity.class));
            } else {
                WebserviceManager.toastErrorByReturnCode(this, returnCode);
                offlineStart();
            }
        } else {
            offlineStart();
        }
    }

    // Set offline mode on true and start activity "NavigationDrawerActivity"
    public void offlineStart() {
        mOfflineMode = true;
        startActivity(new Intent(WelcomeActivity.this, NavigationDrawerActivity.class));
    }

    // Set the buttons for "login" and "register"
    private void setButtons() {
        mButtonLogin = (Button) findViewById(R.id.welcome_buttonLogin);
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            }
        });

        mButtonRegister = (Button) findViewById(R.id.welcome_buttonRegister);
        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                startActivity(new Intent(WelcomeActivity.this, RegisterActivity.class));
            }
        });
    }

    // Checks if the intent is triggered by nfc data detected
    private boolean isNFCAction() {
        return getIntent().getAction().equals(NfcAdapter.ACTION_NDEF_DISCOVERED);
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean showProgress, final boolean hideButtons) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mButtonLogin.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
            mButtonLogin.animate().setDuration(shortAnimTime).alpha(
                    hideButtons ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mButtonLogin.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
                }
            });

            mButtonRegister.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
            mButtonRegister.animate().setDuration(shortAnimTime).alpha(
                    hideButtons ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mButtonRegister.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(showProgress ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    showProgress ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(showProgress ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(showProgress ? View.VISIBLE : View.GONE);
            mButtonLogin.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
            mButtonRegister.setVisibility(hideButtons ? View.GONE : View.VISIBLE);
        }
    }

    private void prepare() {
    }

    public static class TrustManager implements javax.net.ssl.X509TrustManager {
        private static final java.security.cert.X509Certificate[] _AcceptedIssuers = new java.security.cert.X509Certificate[] {};

        public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                throws CertificateException {
        }

        public boolean isClientTrusted(X509Certificate[] chain) {
            return (true);
        }

        public boolean isServerTrusted(X509Certificate[] chain) {
            return (true);
        }

        @Override
        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {

        }

        @Override
        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws java.security.cert.CertificateException {

        }

        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return (_AcceptedIssuers);
        }
    }

    /**
     * Asynchronous Task for loading certificate
     */
    public class LoadCertificate extends AsyncTask<Void, Void, Boolean> {

        private javax.net.ssl.TrustManager[] mTrustManagers;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadCertificate();
            } catch (Exception e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mLoadCertificateTask = null;
        }

        @Override
        protected void onCancelled() {
            mLoadCertificateTask = null;
        }

        private void loadCertificate() throws Exception {
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            // Create TrustManager trusting the Certificates in KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);
            javax.net.ssl.HttpsURLConnection
                    .setDefaultHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    });
            javax.net.ssl.SSLContext context = null;

            if (mTrustManagers == null) {
                mTrustManagers = new javax.net.ssl.TrustManager[]{new TrustManager()};
            }

            try {
                context = javax.net.ssl.SSLContext.getInstance("SSLv3");
                context.init(null, mTrustManagers, new SecureRandom());
            } catch (NoSuchAlgorithmException e) {
                Log.e("allowAllSSL", e.toString());
            } catch (KeyManagementException e) {
                Log.e("allowAllSSL", e.toString());
            }
            javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        }
    }
}